# Projet_Approfondissement_Prog

# Étapes pour tester l'application :
# Section Inventaire:
1. Ouvrir l'application à partir du fichier : ..\bin\Debug\netcoreapp3.1\GestionnaireCuisine.exe
2. Accéder à la fenêtre d'inventaire en selectionnant l'option "Inventaire" du menu principale.
3. Ajouter un Aliment en selectionnant l'option Ajouter Aliment de la fenêtre Inventaire.
4. Selectionner un Aliment dans la liste.
5. Supprimer un Aliment  en selectionnant l'option Supprimer Aliment de la fenetre Inventaire.
6. Selectionner un Aliment de nouveau.
7. Modifier la quantité de l'aliment en modifiant la zone de texte numérique avec les flèches de haut et de bas.
8. Quitter la fenêtre inventaire.
9. Quitter l'application.
10. Répéter les étapes 1 et 2 qui permettront de vérifier la  persistence des données.

# Section Livre de recette:
1. Ouvrir l'application à partir du fichier : ..\bin\Debug\netcoreapp3.1\GestionnaireCuisine.exe
2. Accéder à la fenêtre du livre de recette en selectionnant l'option "Livre de recette" du menu principale.
3. Ajouter une recette en selectionnant l'option "Créer une recette" de la fenêtre Livre de Recettes.
4. Remplissez le champ "Nom" avec le nom de la recette que vous désirez créer.
5. Ajouter les ingrédients voulue en selectionnant un ingrédients dans la liste des ingrédients disponibles en ajustant la quantité nécessaire dans la zone d'écriture du même nom et appuyer sur le bouton "Ajouter ingrédient à la recette"
6.  (Optionel) Appuyer sur le bouton Importer Image pour ajouter une image à votre recette.
7. Quitter la fenêtre et sauvegarder la recette en appuyant sur le bouton "Sauvegarder Recette".
8. Quitter l'application.
9. Répéter les étapes 1 et 2 qui permettront de vérifier la persistence des données.

# Section Liste de production:
1. Ouvrir l'application à partir du fichier : ..\bin\Debug\netcoreapp3.1\GestionnaireCuisine.exe
2. Accéder à la fenêtre de la liste de production en selectionnant l'option "Liste de production" du menu principale.
3. Ajouter des recettes à la liste des recettes à produire ainsi que le nombre de recette prévue de recette à produire de cette dernière
4. Appuyer sur la touche "Simuler production des aliments" (cela va simuler la production des aliments nécessaire et vous indiquer les aliments à produire si nécessaire)
5. Vous pouvez vérifier l'inventaire pour vérifier que la production à belle et bien eu lieux et que le stock des aliment des recettes produites ont réduit.
6. Une fois que vous avez une liste de production prête dans la grille "Liste des aliments à produire" vous pouvez cliquer sur Exporter en PDF et choisir le nom et la destination du fichier pdf que vous voulez produire à partir de la liste de production qui à été généré.
7.  Une fois que vous avez reçue la notification que le fichier à bien été créé vous pouvez aller ouvrir le fichier à la destionation de votre choix et vérifier si tout est bien présent.
8. Quitter la fenêtre et confirmer vos changements sur la liste de production en appuyant sur le bouton OK.
9. Quitter l'application.
10. Répéter les étapes 1 et 2 qui permettront de vérifier la persistence des données.
11. Vous pouvez aussi en tout temps appuyer sur le bouton "Réinitialiser liste de production" pour remettre à 0 la liste de production présentement afficher.