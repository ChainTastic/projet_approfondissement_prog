﻿using System;
using System.Windows.Forms;

namespace GestionnaireCuisine
{
    public partial class FenetrePrincipal : Form
    {
        #region Attributs

        private static Gestionnaire _gestionnaire;

        #endregion Attributs

        #region Accesseurs

        public static Gestionnaire LeGestionnaire
        {
            get { return _gestionnaire; }
            set { _gestionnaire = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public FenetrePrincipal()
        {
            InitializeComponent();
            FenetrePrincipal.LeGestionnaire = new Gestionnaire();
            LeGestionnaire.ChargerXml();
        }

        #endregion Constructeurs

        #region Événements

        private void btnInventaire_Click(object sender, EventArgs e)
        {
            FenetreInventaire fenetreInventaire = new FenetreInventaire();
            if (fenetreInventaire.ShowDialog() == DialogResult.OK)
            {
                LeGestionnaire.Inventaire = fenetreInventaire.UnInventaire;
                LeGestionnaire.GenererXml(typeof(Inventaire), LeGestionnaire.Inventaire, "inventaire");
            }

            fenetreInventaire.Dispose();
        }

        private void btnRecette_Click(object sender, EventArgs e)
        {
            if (LeGestionnaire.Inventaire.GetNbStocks > 0)
            {
                l fenetreLivreRecettes = new l();
                if (fenetreLivreRecettes.ShowDialog() == DialogResult.OK)
                {
                    LeGestionnaire.LivreDeRecette = fenetreLivreRecettes.UnLivreRecette;
                    LeGestionnaire.GenererXml(typeof(LivreRecette), LeGestionnaire.LivreDeRecette,
                        "livreDeRecette");
                }

                fenetreLivreRecettes.Dispose();
            }
            else
            {
                MessageBox.Show("Il est impossible de créer de recette sans aliment dans l'inventaire");
            }
        }

        private void btnListeProduction_Click(object sender, EventArgs e)
        {
            if (LeGestionnaire.LivreDeRecette.ListeRecettes.Count > 0)
            {
                if (LeGestionnaire.Inventaire.GetNbStocks > 0)
                {
                    FenetreListeProduction frmListeProduction = new FenetreListeProduction();
                    if (frmListeProduction.ShowDialog() == DialogResult.OK)
                    {
                        LeGestionnaire.ListeDeProduction = frmListeProduction.UneListeDeProduction;
                        LeGestionnaire.GenererXml(typeof(ListeDeProduction),
                            LeGestionnaire.ListeDeProduction, "listeDeProduction");
                        LeGestionnaire.GenererXml(typeof(Inventaire), LeGestionnaire.Inventaire, "inventaire");
                    }

                    frmListeProduction.Dispose();
                }
                else
                {
                    MessageBox.Show(
                        "Il est impossible de produire une liste de production sans aliment dans l'inventaire");
                }
            }
            else
            {
                MessageBox.Show("Il est impossible de produire une liste de production sans recette");
            }
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion Événements

        private void button1_Click(object sender, EventArgs e)
        {
            FenetreTutoriel tutoriel = new FenetreTutoriel();
            if (tutoriel.ShowDialog() == DialogResult.OK)
            {
                tutoriel.Dispose();
            }
        }
    }
}