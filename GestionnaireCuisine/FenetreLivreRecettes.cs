﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using GestionnaireCuisine.Properties;

namespace GestionnaireCuisine
{
    public partial class l : Form
    {
        #region Attributs

        private LivreRecette _leLivreDeRecette;
        private ImageList _images;

        #endregion Attributs

        #region Accesseurs

        public LivreRecette UnLivreRecette
        {
            get { return _leLivreDeRecette; }
            set { _leLivreDeRecette = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public l()
        {
            InitializeComponent();
            FenetrePrincipal.LeGestionnaire.ChargerXml();
            if (File.Exists(Gestionnaire.CheminLivreRecette))
            {
                _leLivreDeRecette = FenetrePrincipal.LeGestionnaire.LivreDeRecette;
            }
            else
            {
                _leLivreDeRecette = new LivreRecette();
            }

            AcceptButton = btnOK;
            CancelButton = btnAnnuler;
            btnOK.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
            _images = new ImageList {ImageSize = new Size(32, 32)};
            lstVwLivreRecette.View = View.Details;
            lstVwLivreRecette.Columns.Add("Recettes", -2, HorizontalAlignment.Left);
        }

        #endregion Constructeurs

        #region Événements

        private void FenetreLivreRecettes_Load(object sender, EventArgs e)
        {
            lstVwLivreRecette.GridLines = true;
            lstVwLivreRecette.FullRowSelect = true;
            lstVwIngredients.GridLines = true;
            lstVwIngredients.FullRowSelect = true;
            RemplirLivreRecette(_leLivreDeRecette.ListeRecettes);
        }

        private void btnCreerRecette_Click(object sender, EventArgs e)
        {
            FenetreRecette fenetreRecette = new FenetreRecette();
            if (fenetreRecette.ShowDialog() == DialogResult.OK)
            {
                Recette nouvelleRecette = fenetreRecette.LaRecette;

                string cheminImage = nouvelleRecette.NomImage;

                try
                {
                    _images.Images.Add(Image.FromFile(cheminImage));
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }

                lstVwLivreRecette.SmallImageList = _images;
                lstVwLivreRecette.Items.Add(fenetreRecette.LaRecette.Nom);
                lstVwLivreRecette.Items[^1].ImageIndex = _images.Images.Count - 1;

                _leLivreDeRecette.AjouterRecette(nouvelleRecette);
            }

            fenetreRecette.Dispose();
        }

        //ToDo factorisé code si possible
        private void btnModifierRecette_Click(object sender, EventArgs e)
        {
            if (lstVwLivreRecette.SelectedItems.Count == 1)
            {
                FenetreRecette fenetreModifRecette = new FenetreRecette();
                fenetreModifRecette.Text = "Modification d'une recette";

                string nomRecette = lstVwLivreRecette.SelectedItems[0].Text;
                int iIndexRecetteSelectionne = lstVwLivreRecette.Items.IndexOf(lstVwLivreRecette.SelectedItems[0]);
                fenetreModifRecette.LaRecette = _leLivreDeRecette.ObtenirRecetteSelonNom(nomRecette);
                string cheminImage = fenetreModifRecette.LaRecette.NomImage;
                if (fenetreModifRecette.ShowDialog() == DialogResult.OK)
                {
                    Recette recetteModifie = fenetreModifRecette.LaRecette;
                    string cheminImageNouvelle = recetteModifie.NomImage;
                    if (cheminImageNouvelle != cheminImage)
                    {
                        try
                        {
                            _images.Images.Add(Image.FromFile(cheminImageNouvelle));
                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message);
                        }

                        lstVwLivreRecette.SmallImageList = _images;
                        lstVwLivreRecette.Items[iIndexRecetteSelectionne].ImageIndex = _images.Images.Count - 1;
                    }

                    RemplirLivreRecette(_leLivreDeRecette.ListeRecettes);
                    lstVwLivreRecette.Items[iIndexRecetteSelectionne].Selected = true;
                    UpdateListeIngredient();
                }

                fenetreModifRecette.Dispose();
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner une recette pour pouvoir la modifier.", "Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSupprimerRecette_Click(object sender, EventArgs e)
        {
            if (lstVwLivreRecette.SelectedItems.Count == 1)
            {
                ListViewItem recetteSelectionne = lstVwLivreRecette.SelectedItems[0];
                string strNomRecette = recetteSelectionne.SubItems[0].Text;
                Recette recette = _leLivreDeRecette.ObtenirRecetteSelonNom(strNomRecette);
                _leLivreDeRecette.RetirerRecette(recette);
                lstVwLivreRecette.Items.Remove(recetteSelectionne);
                lstVwIngredients.Items.Clear();
            }
        }

        private void lstVwLivreRecette_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstVwLivreRecette.SelectedItems.Count == 1)
            {
                UpdateListeIngredient();
            }
        }

        private void txtFiltreRecetteSelonAliment_TextChanged(object sender, EventArgs e)
        {
            List<Recette> resultatRecherche =
                UnLivreRecette.ObtenirRecettesSelonAliment(txtFiltreRecetteSelonAliment.Text);
            RemplirLivreRecette(resultatRecherche);
        }

        private void txtFiltreSelonNomRecette_TextChanged(object sender, EventArgs e)
        {
            List<Recette> resultatRecherche = UnLivreRecette.ObtenirRecettesSelonNom(txtFiltreSelonNomRecette.Text);
            RemplirLivreRecette(resultatRecherche);
        }

        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Méthode permettant d'afficher chaque recette présente dans la liste de recette de l'objet Livre de recette
        /// </summary>
        private void RemplirLivreRecette(List<Recette> pListeRecettes)
        {
            string recetteSelectionne = null;
            if (lstVwLivreRecette.SelectedItems.Count > 0)
            {
                recetteSelectionne = lstVwLivreRecette.SelectedItems[0].Text;
            }
            

            Dictionary<string, string> imageDictionary = new Dictionary<string, string>();
            lstVwLivreRecette.Items.Clear();
            this._images.Images.Add("livreDeCuisine", Resources.livreDeCuisine);
            imageDictionary.Add("livreDeCuisine", Resources.livreDeCuisine.ToString());
            string cheminImages = "..//..//..//Images";
            try
            {
                IEnumerable<string> fichiers = Directory.EnumerateFiles(cheminImages);
                foreach (string fichier in fichiers)
                {
                    if (fichier.Contains("git")) continue;
                    Image image = Image.FromFile(fichier);
                    image.Tag = fichier;
                    _images.Images.Add(Path.GetFileNameWithoutExtension(fichier), image);
                    imageDictionary.Add(Path.GetFileNameWithoutExtension(fichier), fichier);
                    lstVwLivreRecette.SmallImageList = _images;
                    image.Dispose();
                }

                foreach (Recette recette in pListeRecettes)
                {
                    ListViewItem lvi = new ListViewItem(recette.Nom);
                    foreach (var elem in imageDictionary)
                    {
                        if (recette.NomImage.Contains(elem.Key))
                        {
                            lvi.ImageKey = elem.Key;
                        }
                    }

                    lstVwLivreRecette.Items.Add(lvi);
                }

                lstVwLivreRecette.Refresh();
                if (!string.IsNullOrEmpty(recetteSelectionne))
                {
                    foreach (ListViewItem listViewItem in lstVwLivreRecette.Items)
                    {
                        if (listViewItem.Text == recetteSelectionne)
                        {
                            listViewItem.Selected = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Méthode permettant de mettre à jour la liste des ingrédients de la recette selectionné
        /// </summary>
        private void UpdateListeIngredient()
        {
            lstVwIngredients.Items.Clear();
            string nomRecette = lstVwLivreRecette.SelectedItems[0].Text;
            Recette recetteSelectionnee = _leLivreDeRecette.ObtenirRecetteSelonNom(nomRecette);
            this.lblListeIngredientsRecette.Text =
                string.Format("Ingrédients de la recette : {0}", recetteSelectionnee.Nom);
            this.lblListeIngredientsRecette.Visible = true;
            foreach ((Aliment, float, UniteMesure) tupleAlimentQtty in recetteSelectionnee.LstAlimentsQtty)
            {
                string[] arrInfoAliment = new string[2];
                arrInfoAliment[0] = tupleAlimentQtty.Item1.Nom;
                arrInfoAliment[1] = String.Format("{0} {1}",
                    tupleAlimentQtty.Item2.ToString(CultureInfo.CurrentCulture),
                    FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(tupleAlimentQtty.Item3));

                ListViewItem item = new ListViewItem(arrInfoAliment);
                lstVwIngredients.Items.Add(item);
            }
        }

        #endregion Méthodes


    }
}