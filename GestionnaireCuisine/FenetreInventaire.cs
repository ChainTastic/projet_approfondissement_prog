﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Media;
using System.Windows.Forms;
using GestionnaireCuisine.Properties;

namespace GestionnaireCuisine
{
    public partial class FenetreInventaire : Form
    {
        #region Attributs

        private Inventaire _lInventaire;
        private ListViewColumnSorter lvColumnSorter;

        #endregion Attributs

        #region Accesseurs

        public Inventaire UnInventaire
        {
            get { return _lInventaire; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public FenetreInventaire()
        {
            InitializeComponent();
            this.lvColumnSorter = new ListViewColumnSorter();
            this.lstInventaire.ListViewItemSorter = this.lvColumnSorter;
            FenetrePrincipal.LeGestionnaire.ChargerXml();
            if (File.Exists(Gestionnaire.CheminInventaire))
            {
                _lInventaire = FenetrePrincipal.LeGestionnaire.Inventaire;
            }
            else
            {
                _lInventaire = new Inventaire();
            }

            AcceptButton = btnOK;
            CancelButton = btnAnnuler;
            btnOK.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
            ManageCheckGroupBox(this.chkBoxFiltreBarreRecherche, this.gboFiltreBarreRecherche);
            ManageCheckGroupBox(this.chkBoxFiltreStatusAliment, this.gboFiltreSelonStatut);
            ManageCheckGroupBox(this.chkBoxFiltreTypeAliment, this.gboFiltreTypeAliment);
            RemplirFiltres();
        }

        #endregion Constructeurs

        #region Événements

        private void btnAjouterAliment_Click(object sender, EventArgs e)
        {
            FenetreAliment fenetreAliment = new FenetreAliment { Text = "Ajout d'un aliment" };
            if (fenetreAliment.ShowDialog() == DialogResult.OK)
            {
                Stock stock = new Stock(fenetreAliment.UnAliment, fenetreAliment.QuantiteAliment,
                    fenetreAliment.UniteMesure);
                string[] arrInfoAliment = new string[4];
                arrInfoAliment[0] = fenetreAliment.UnAliment.Nom;
                arrInfoAliment[1] = stock.Status.ToString();
                (float, UniteMesure) qtty =
                    FenetrePrincipal.LeGestionnaire.ObtenirQuantiteOptimale(stock.Quantite, stock.UniteMesure);
                arrInfoAliment[2] = string.Format("{0} {1}", qtty.Item1,
                    FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(qtty.Item2));
                arrInfoAliment[3] = fenetreAliment.UnAliment.ObtenirTempsConservationRestant().ToString();
                ListViewItem item = new ListViewItem(arrInfoAliment);
                lstInventaire.Items.Add(item);
                _lInventaire.AjouterStock(stock);
                VerifierAliment(lstInventaire.Items.IndexOf(item));
            }

            fenetreAliment.Dispose();
        }

        private void FenetreInventaire_Load(object sender, EventArgs e)
        {
            lstInventaire.GridLines = true;
            lstInventaire.FullRowSelect = true;

            RemplirInventaire(this._lInventaire.LstStock);
        }

        private void btnSupprimerAliment_Click(object sender, EventArgs e)
        {
            if (lstInventaire.SelectedItems.Count > 0)
            {
                ListViewItem item = lstInventaire.SelectedItems[0];
                string nomAliment = item.SubItems[0].Text;
                for (int i = 0; i < this._lInventaire.LstStock.Count; i++)
                {
                    Stock stock = this._lInventaire.LstStock[i];
                    if (stock.Aliment.Nom == nomAliment)
                    {
                        _lInventaire.RetirerStock(stock);
                        lstInventaire.Items.Remove(item);
                    }
                }
            }
            else
            {
                MessageBox.Show("Vous devez selectionner un aliment pour pouvoir le supprimer.");
            }
        }

        private void lstInventaire_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstInventaire.SelectedItems.Count > 0)
            {
                numQttyAliment.Value = Convert.ToDecimal(lstInventaire.SelectedItems[0].SubItems[2].Text.Split(" ")[0]);
            }
        }


        private void numQttyAliment_Leave(object sender, EventArgs e)
        {
            if (lstInventaire.SelectedItems.Count > 0)
            {
                int indiceAliment = lstInventaire.Items.IndexOf(lstInventaire.SelectedItems[0]);
                if (numQttyAliment.Value != (decimal)_lInventaire.GetStockSelonAliment(this._lInventaire.ObtenirAlimentSelonNom(this.lstInventaire.Items[indiceAliment].SubItems[0].Text)).Quantite)
                {
                    this._lInventaire.GetStockSelonAliment(
                            this._lInventaire.ObtenirAlimentSelonNom(this.lstInventaire.Items[indiceAliment].SubItems[0].Text))
                        .Quantite = (float)numQttyAliment.Value;
                    lstInventaire.SelectedItems[0].SubItems[2].Text = _lInventaire.GetStockSelonAliment(this._lInventaire.ObtenirAlimentSelonNom(this.lstInventaire.Items[indiceAliment].SubItems[0].Text)).QuantiteFormatte;
                    VerifierAliment(indiceAliment);
                }
            }
        }

        private void chkBoxFiltreBarreRecherche_CheckedChanged(object sender, EventArgs e)
        {
            ManageCheckGroupBox(this.chkBoxFiltreBarreRecherche, this.gboFiltreBarreRecherche);
        }

        private void chkBoxFiltreTypeAliment_CheckedChanged(object sender, EventArgs e)
        {
            ManageCheckGroupBox(this.chkBoxFiltreTypeAliment, this.gboFiltreTypeAliment);
        }

        private void chkBoxFiltreStatusAliment_CheckedChanged(object sender, EventArgs e)
        {
            ManageCheckGroupBox(this.chkBoxFiltreStatusAliment, this.gboFiltreSelonStatut);
            if (!this.chkBoxFiltreStatusAliment.Checked)
            {
                foreach (RadioButton button in this.gboFiltreSelonStatut.Controls)
                {
                    button.Checked = false;
                }
            }
        }

        private void btnFiltrer_Click(object sender, EventArgs e)
        {
            if (ObtenirFiltreSelectionne().Count > 0)
            {
                RemplirInventaire(_lInventaire.ObtenirListeSelonFiltres(ObtenirFiltreSelectionne()));
            }
            else
            {
                MessageBox.Show("Vous devez choisir un filtre avant de pouvoir filtrer");
            }
        }

        private void btnReinitialiserFiltres_Click(object sender, EventArgs e)
        {
            chkBoxFiltreBarreRecherche.Checked = false;
            chkBoxFiltreStatusAliment.Checked = false;
            chkBoxFiltreTypeAliment.Checked = false;
            RemplirInventaire(this._lInventaire.LstStock);
        }

        private void lstInventaire_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListViewColumnSorter sorter = (ListViewColumnSorter)this.lstInventaire.ListViewItemSorter;
            sorter.SortColumn = e.Column;

            if (lvColumnSorter.Order == SortOrder.Ascending)
            {
                lvColumnSorter.Order = SortOrder.Descending;
            }
            else
            {
                lvColumnSorter.Order = SortOrder.Ascending;
            }
            lstInventaire.Sort();
        }

        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Méthode servant à vérifier le status de chaque aliment dans l'inventaire
        /// </summary>
        private void VerifierStatusAliments()
        {
            for (int i = 0; i < lstInventaire.Items.Count; i++)
            {
                VerifierAliment(i);
            }
        }
        /// <summary>
        /// Méthode permettant de vérifier le status de l'aliment à l'indice reçue en paramètre
        /// </summary>
        /// <param name="pIndice">indice de l'aliment dans le listview</param>
        private void VerifierAliment(int pIndice)
        {
            ListViewItem itemAliment = lstInventaire.Items[pIndice];
            int joursRestants = Convert.ToInt32(itemAliment.SubItems[3].Text);
            float qttyRestants = (float) Convert.ToDecimal(itemAliment.SubItems[2].Text.Split(" ")[0]);
            Stock stock = _lInventaire.GetStockSelonAliment(_lInventaire.ObtenirAlimentSelonNom(lstInventaire.Items[pIndice].SubItems[0].Text));
            if (qttyRestants > 0)
            {
                if (joursRestants <= 0)
                {
                    itemAliment.BackColor = Color.Red;
                    FenetreInventaire.JouerAlerteErreur();
                    MettreAJourStatus(Status.Perime, stock, pIndice);
                }
                else if (joursRestants == 1)
                {
                    MettreAJourStatus(Status.Disponible,stock, pIndice);
                    itemAliment.BackColor = Color.Yellow;
                }
                else
                {
                    MettreAJourStatus(Status.Disponible,stock, pIndice);
                    itemAliment.BackColor = Color.Green;
                }
            }
            else
            {
                itemAliment.BackColor = Color.OrangeRed;
                MettreAJourStatus(Status.NonDisponible, stock, pIndice);
            }
        }
        /// <summary>
        /// Méthode permettant de mettre à jour le statut visuelle et
        /// celui du stock en paramètre selon le statut et l'indice reçue
        /// </summary>
        /// <param name="pStatus">Status de la mise à jour</param>
        /// <param name="pStock">Stock à mettre à jour</param>
        /// <param name="pIndice">indice de l'item à mettre à jour dans le listview</param>
        private void MettreAJourStatus(Status pStatus, Stock pStock, int pIndice)
        {
            pStock.Status = pStatus;
            lstInventaire.Items[pIndice].SubItems[1].Text = pStock.Status.ToString();
        }

        /// <summary>
        /// Méthode servant à jouer une alerte sonore d'erreur
        /// </summary>
        private static void JouerAlerteErreur()
        {
            SoundPlayer boiteAMusique = new SoundPlayer(Resources.son_erreur);
            boiteAMusique.Play();
        }

        /// <summary>
        /// Permet de synchroniser l'état du checkbox avec le groupbox
        /// </summary>
        /// <param name="chk">Checkbox</param>
        /// <param name="gbo">GroupeBox</param>
        private void ManageCheckGroupBox(CheckBox chk, GroupBox gbo)
        {
            if (chk.Parent == gbo)
            {
                gbo.Parent.Controls.Add(chk);

                chk.Location = new Point(chk.Left + gbo.Left, chk.Top + gbo.Top);

                chk.BringToFront();
            }

            gbo.Enabled = chk.Checked;
        }

        /// <summary>
        /// Permet de remplir le listView de chaque aliment présent dans la liste de stock reçue en paramètre
        /// </summary>
        /// <param name="pListeStockInventaire">Liste de stock de l'inventaire</param>
        private void RemplirInventaire(List<Stock> pListeStockInventaire)
        {
            this.lstInventaire.Items.Clear();
            string[] arrInfoAliment = new string[4];
            foreach (Stock stock in pListeStockInventaire)
            {
                arrInfoAliment[0] = stock.Aliment.Nom;
                arrInfoAliment[1] = stock.Status.ToString();
                (float, UniteMesure) qtty =
                    FenetrePrincipal.LeGestionnaire.ObtenirQuantiteOptimale(stock.Quantite, stock.UniteMesure);
                arrInfoAliment[2] = string.Format("{0} {1}", qtty.Item1,
                    FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(qtty.Item2));
                arrInfoAliment[3] = stock.Aliment.ObtenirTempsConservationRestant().ToString();
                ListViewItem item = new ListViewItem(arrInfoAliment);
                lstInventaire.Items.Add(item);
            }

            this.lstInventaire.Refresh();
            VerifierStatusAliments();
        }

        /// <summary>
        /// Permet de remplir les groupBox de chaque filtre
        /// </summary>
        private void RemplirFiltres()
        {
            Point pointDeDepart = new Point(7, 26);
            foreach (Status status in (Status[])Enum.GetValues(typeof(Status)))
            {
                RadioButton nouveauRadioButton = new RadioButton
                {
                    Name = string.Format("opt{0}", status),
                    Text = status.ToString(),
                    Location = pointDeDepart
                };
                pointDeDepart.Y += 25;
                this.gboFiltreSelonStatut.Controls.Add(nouveauRadioButton);
            }

            foreach (CategorieAliment categorie in (CategorieAliment[])Enum.GetValues(typeof(CategorieAliment)))
            {
                this.cboTypesAliment.Items.Add(categorie);
            }

            this.cboTypesAliment.SelectedItem = this.cboTypesAliment.Items[0];
            this.gboFiltreSelonStatut.Refresh();
            this.cboTypesAliment.Refresh();
        }

        /// <summary>
        /// Permet d'obtenir le statut qui est sélectionné dans le filtre par l'utilisateur
        /// </summary>
        /// <returns>Le statut sélectionné par l'utilisateur</returns>
        private Status ObtenirStatusSelectionne()
        {
            Status leStatus = Status.Disponible;

            foreach (RadioButton boutonRadio in this.gboFiltreSelonStatut.Controls)
            {
                if (boutonRadio.Checked)
                {
                    switch (boutonRadio.Text)
                    {
                        case "Perime":
                            leStatus = Status.Perime;
                            break;

                        case "NonDisponible":
                            leStatus = Status.NonDisponible;
                            break;

                        default:
                            leStatus = Status.Disponible;
                            break;
                    }
                }
            }

            return leStatus;
        }

        /// <summary>
        /// Permet d'obtenir les filtres qui sont sélectionné par l'utilisateur ainsi que leur valeur voulue
        /// </summary>
        /// <returns>Liste de dictionnaire composé du filtre choisie par l'utilisateur et de sa valeur voulue</returns>
        private ListDictionary ObtenirFiltreSelectionne()
        {
            ListDictionary listeFiltres = new ListDictionary();
            if (this.chkBoxFiltreBarreRecherche.Checked)
            {
                listeFiltres.Add("Barre recherche", this.txtRecherche.Text);
            }

            if (this.chkBoxFiltreTypeAliment.Checked)
            {
                listeFiltres.Add("Catégorie", (CategorieAliment)this.cboTypesAliment.SelectedItem);
            }

            if (this.chkBoxFiltreStatusAliment.Checked)
            {
                listeFiltres.Add("Statut", ObtenirStatusSelectionne());
            }

            return listeFiltres;
        }


        #endregion Méthodes


    }
}