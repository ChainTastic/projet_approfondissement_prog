﻿namespace GestionnaireCuisine
{
    partial class FenetreRecette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNomRecette = new System.Windows.Forms.TextBox();
            this.pbRecette = new System.Windows.Forms.PictureBox();
            this.btnImporterImage = new System.Windows.Forms.Button();
            this.lblIngredientsDisponible = new System.Windows.Forms.Label();
            this.lblNomRecette = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnAjouterIngredient = new System.Windows.Forms.Button();
            this.numQttyIngredientNecessaire = new System.Windows.Forms.NumericUpDown();
            this.lblQttyAlimentNecessaire = new System.Windows.Forms.Label();
            this.lblIngrédientRecette = new System.Windows.Forms.Label();
            this.btnRetirerIngredient = new System.Windows.Forms.Button();
            this.lstVwIngredientsRecette = new System.Windows.Forms.ListView();
            this.colIngredientRecetteNom = new System.Windows.Forms.ColumnHeader();
            this.colIngredientRecetteQtty = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.lstVwIngredientDispo = new System.Windows.Forms.ListView();
            this.colIngredientDispoNom = new System.Windows.Forms.ColumnHeader();
            this.colIngredientDispoQtty = new System.Windows.Forms.ColumnHeader();
            this.cbBoxUniteMesure = new System.Windows.Forms.ComboBox();
            this.lblUniteMesure = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbRecette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQttyIngredientNecessaire)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNomRecette
            // 
            this.txtNomRecette.Location = new System.Drawing.Point(12, 61);
            this.txtNomRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtNomRecette.Name = "txtNomRecette";
            this.txtNomRecette.Size = new System.Drawing.Size(100, 23);
            this.txtNomRecette.TabIndex = 0;
            // 
            // pbRecette
            // 
            this.pbRecette.Location = new System.Drawing.Point(12, 90);
            this.pbRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pbRecette.Name = "pbRecette";
            this.pbRecette.Size = new System.Drawing.Size(99, 72);
            this.pbRecette.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbRecette.TabIndex = 1;
            this.pbRecette.TabStop = false;
            // 
            // btnImporterImage
            // 
            this.btnImporterImage.Location = new System.Drawing.Point(12, 168);
            this.btnImporterImage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnImporterImage.Name = "btnImporterImage";
            this.btnImporterImage.Size = new System.Drawing.Size(99, 24);
            this.btnImporterImage.TabIndex = 2;
            this.btnImporterImage.Text = "Importer Image";
            this.btnImporterImage.UseVisualStyleBackColor = true;
            this.btnImporterImage.Click += new System.EventHandler(this.btnImporterImage_Click);
            // 
            // lblIngredientsDisponible
            // 
            this.lblIngredientsDisponible.AutoSize = true;
            this.lblIngredientsDisponible.Location = new System.Drawing.Point(118, 25);
            this.lblIngredientsDisponible.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIngredientsDisponible.Name = "lblIngredientsDisponible";
            this.lblIngredientsDisponible.Size = new System.Drawing.Size(177, 15);
            this.lblIngredientsDisponible.TabIndex = 4;
            this.lblIngredientsDisponible.Text = "Liste des ingrédients disponibles";
            // 
            // lblNomRecette
            // 
            this.lblNomRecette.AutoSize = true;
            this.lblNomRecette.Location = new System.Drawing.Point(12, 43);
            this.lblNomRecette.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomRecette.Name = "lblNomRecette";
            this.lblNomRecette.Size = new System.Drawing.Size(40, 15);
            this.lblNomRecette.TabIndex = 5;
            this.lblNomRecette.Text = "Nom :";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(542, 228);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(93, 40);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "Sauvegarder recette";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(443, 228);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(93, 39);
            this.btnAnnuler.TabIndex = 8;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // btnAjouterIngredient
            // 
            this.btnAjouterIngredient.Location = new System.Drawing.Point(323, 121);
            this.btnAjouterIngredient.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnAjouterIngredient.Name = "btnAjouterIngredient";
            this.btnAjouterIngredient.Size = new System.Drawing.Size(117, 42);
            this.btnAjouterIngredient.TabIndex = 9;
            this.btnAjouterIngredient.Text = "Ajouter ingrédient à la recette";
            this.btnAjouterIngredient.UseVisualStyleBackColor = true;
            this.btnAjouterIngredient.Click += new System.EventHandler(this.btnAjouterIngredient_Click);
            // 
            // numQttyIngredientNecessaire
            // 
            this.numQttyIngredientNecessaire.DecimalPlaces = 3;
            this.numQttyIngredientNecessaire.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numQttyIngredientNecessaire.Location = new System.Drawing.Point(378, 52);
            this.numQttyIngredientNecessaire.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.numQttyIngredientNecessaire.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numQttyIngredientNecessaire.Name = "numQttyIngredientNecessaire";
            this.numQttyIngredientNecessaire.Size = new System.Drawing.Size(71, 23);
            this.numQttyIngredientNecessaire.TabIndex = 10;
            this.numQttyIngredientNecessaire.ValueChanged += new System.EventHandler(this.numQttyIngredientNecessaire_ValueChanged);
            // 
            // lblQttyAlimentNecessaire
            // 
            this.lblQttyAlimentNecessaire.AutoSize = true;
            this.lblQttyAlimentNecessaire.Location = new System.Drawing.Point(312, 54);
            this.lblQttyAlimentNecessaire.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQttyAlimentNecessaire.Name = "lblQttyAlimentNecessaire";
            this.lblQttyAlimentNecessaire.Size = new System.Drawing.Size(62, 15);
            this.lblQttyAlimentNecessaire.TabIndex = 11;
            this.lblQttyAlimentNecessaire.Text = "Quantité : ";
            // 
            // lblIngrédientRecette
            // 
            this.lblIngrédientRecette.AutoSize = true;
            this.lblIngrédientRecette.Location = new System.Drawing.Point(479, 25);
            this.lblIngrédientRecette.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIngrédientRecette.Name = "lblIngrédientRecette";
            this.lblIngrédientRecette.Size = new System.Drawing.Size(139, 15);
            this.lblIngrédientRecette.TabIndex = 13;
            this.lblIngrédientRecette.Text = "Ingrédients de la recette :";
            // 
            // btnRetirerIngredient
            // 
            this.btnRetirerIngredient.Location = new System.Drawing.Point(323, 180);
            this.btnRetirerIngredient.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnRetirerIngredient.Name = "btnRetirerIngredient";
            this.btnRetirerIngredient.Size = new System.Drawing.Size(117, 42);
            this.btnRetirerIngredient.TabIndex = 14;
            this.btnRetirerIngredient.Text = "Retirer ingrédient de la recette";
            this.btnRetirerIngredient.UseVisualStyleBackColor = true;
            this.btnRetirerIngredient.Click += new System.EventHandler(this.btnRetirerIngredientRecette_Click);
            // 
            // lstVwIngredientsRecette
            // 
            this.lstVwIngredientsRecette.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIngredientRecetteNom,
            this.colIngredientRecetteQtty});
            this.lstVwIngredientsRecette.HideSelection = false;
            this.lstVwIngredientsRecette.Location = new System.Drawing.Point(456, 54);
            this.lstVwIngredientsRecette.Name = "lstVwIngredientsRecette";
            this.lstVwIngredientsRecette.Size = new System.Drawing.Size(180, 168);
            this.lstVwIngredientsRecette.TabIndex = 15;
            this.lstVwIngredientsRecette.UseCompatibleStateImageBehavior = false;
            this.lstVwIngredientsRecette.View = System.Windows.Forms.View.Details;
            this.lstVwIngredientsRecette.SelectedIndexChanged += new System.EventHandler(this.lstVwIngredientsRecette_SelectedIndexChanged);
            // 
            // colIngredientRecetteNom
            // 
            this.colIngredientRecetteNom.Text = "Nom";
            this.colIngredientRecetteNom.Width = 115;
            // 
            // colIngredientRecetteQtty
            // 
            this.colIngredientRecetteQtty.Text = "Quantité";
            this.colIngredientRecetteQtty.Width = 70;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nom";
            this.columnHeader3.Width = 115;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Quantité";
            this.columnHeader4.Width = 75;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Nom";
            this.columnHeader5.Width = 115;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Quantité";
            this.columnHeader6.Width = 75;
            // 
            // lstVwIngredientDispo
            // 
            this.lstVwIngredientDispo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIngredientDispoNom,
            this.colIngredientDispoQtty});
            this.lstVwIngredientDispo.HideSelection = false;
            this.lstVwIngredientDispo.Location = new System.Drawing.Point(125, 54);
            this.lstVwIngredientDispo.Name = "lstVwIngredientDispo";
            this.lstVwIngredientDispo.Size = new System.Drawing.Size(180, 168);
            this.lstVwIngredientDispo.TabIndex = 15;
            this.lstVwIngredientDispo.UseCompatibleStateImageBehavior = false;
            this.lstVwIngredientDispo.View = System.Windows.Forms.View.Details;
            this.lstVwIngredientDispo.SelectedIndexChanged += new System.EventHandler(this.lstVwIngredientDispo_SelectedIndexChanged);
            // 
            // colIngredientDispoNom
            // 
            this.colIngredientDispoNom.Text = "Nom";
            this.colIngredientDispoNom.Width = 115;
            // 
            // colIngredientDispoQtty
            // 
            this.colIngredientDispoQtty.Text = "Quantité";
            this.colIngredientDispoQtty.Width = 70;
            // 
            // cbBoxUniteMesure
            // 
            this.cbBoxUniteMesure.FormattingEnabled = true;
            this.cbBoxUniteMesure.Location = new System.Drawing.Point(378, 81);
            this.cbBoxUniteMesure.Name = "cbBoxUniteMesure";
            this.cbBoxUniteMesure.Size = new System.Drawing.Size(72, 23);
            this.cbBoxUniteMesure.TabIndex = 16;
            // 
            // lblUniteMesure
            // 
            this.lblUniteMesure.AutoSize = true;
            this.lblUniteMesure.Location = new System.Drawing.Point(312, 84);
            this.lblUniteMesure.Name = "lblUniteMesure";
            this.lblUniteMesure.Size = new System.Drawing.Size(41, 15);
            this.lblUniteMesure.TabIndex = 17;
            this.lblUniteMesure.Text = "Unité :";
            // 
            // FenetreRecette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 280);
            this.Controls.Add(this.lblUniteMesure);
            this.Controls.Add(this.cbBoxUniteMesure);
            this.Controls.Add(this.lstVwIngredientDispo);
            this.Controls.Add(this.lstVwIngredientsRecette);
            this.Controls.Add(this.btnRetirerIngredient);
            this.Controls.Add(this.lblIngrédientRecette);
            this.Controls.Add(this.lblQttyAlimentNecessaire);
            this.Controls.Add(this.numQttyIngredientNecessaire);
            this.Controls.Add(this.btnAjouterIngredient);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblNomRecette);
            this.Controls.Add(this.lblIngredientsDisponible);
            this.Controls.Add(this.btnImporterImage);
            this.Controls.Add(this.pbRecette);
            this.Controls.Add(this.txtNomRecette);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "FenetreRecette";
            this.Text = "FenetreRecette";
            this.Load += new System.EventHandler(this.FenetreRecette_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbRecette)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numQttyIngredientNecessaire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnRetirerIngredient;

        #endregion

        private System.Windows.Forms.TextBox txtNomRecette;
        private System.Windows.Forms.PictureBox pbRecette;
        private System.Windows.Forms.Button btnImporterImage;
        private System.Windows.Forms.Label lblIngredientsDisponible;
        private System.Windows.Forms.Label lblNomRecette;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnAjouterIngredient;
        private System.Windows.Forms.NumericUpDown numQttyIngredientNecessaire;
        private System.Windows.Forms.Label lblQttyAlimentNecessaire;
        private System.Windows.Forms.Label lblIngrédientRecette;
        private System.Windows.Forms.ListView lstVwIngredientsRecette;
        private System.Windows.Forms.ColumnHeader colIngredientRecetteNom;
        private System.Windows.Forms.ColumnHeader colIngredientRecetteQtty;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView lstVwIngredientDispo;
        private System.Windows.Forms.ColumnHeader colIngredientDispoNom;
        private System.Windows.Forms.ColumnHeader colIngredientDispoQtty;
        private System.Windows.Forms.ComboBox cbBoxUniteMesure;
        private System.Windows.Forms.Label lblUniteMesure;
    }
}