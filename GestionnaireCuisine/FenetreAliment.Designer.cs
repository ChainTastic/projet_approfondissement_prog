﻿namespace GestionnaireCuisine
{
    partial class FenetreAliment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNomAliment = new System.Windows.Forms.Label();
            this.txtNomAliment = new System.Windows.Forms.TextBox();
            this.txtDateReception = new System.Windows.Forms.TextBox();
            this.lblDateReception = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.gboCategorieAliment = new System.Windows.Forms.GroupBox();
            this.optDivers = new System.Windows.Forms.RadioButton();
            this.optOeuf = new System.Windows.Forms.RadioButton();
            this.optCerealier = new System.Windows.Forms.RadioButton();
            this.optLaitier = new System.Windows.Forms.RadioButton();
            this.optViandeEtSub = new System.Windows.Forms.RadioButton();
            this.optFruitEtLegume = new System.Windows.Forms.RadioButton();
            this.txtQttyAliment = new System.Windows.Forms.NumericUpDown();
            this.lblQttyAliment = new System.Windows.Forms.Label();
            this.cbBoxUniteMesure = new System.Windows.Forms.ComboBox();
            this.lblUniteMesure = new System.Windows.Forms.Label();
            this.gboCategorieAliment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQttyAliment)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomAliment
            // 
            this.lblNomAliment.AutoSize = true;
            this.lblNomAliment.Location = new System.Drawing.Point(12, 85);
            this.lblNomAliment.Name = "lblNomAliment";
            this.lblNomAliment.Size = new System.Drawing.Size(40, 15);
            this.lblNomAliment.TabIndex = 0;
            this.lblNomAliment.Text = "Nom :";
            // 
            // txtNomAliment
            // 
            this.txtNomAliment.Location = new System.Drawing.Point(12, 103);
            this.txtNomAliment.Name = "txtNomAliment";
            this.txtNomAliment.Size = new System.Drawing.Size(155, 23);
            this.txtNomAliment.TabIndex = 1;
            // 
            // txtDateReception
            // 
            this.txtDateReception.Enabled = false;
            this.txtDateReception.Location = new System.Drawing.Point(12, 39);
            this.txtDateReception.Name = "txtDateReception";
            this.txtDateReception.Size = new System.Drawing.Size(82, 23);
            this.txtDateReception.TabIndex = 4;
            // 
            // lblDateReception
            // 
            this.lblDateReception.AutoSize = true;
            this.lblDateReception.Location = new System.Drawing.Point(12, 21);
            this.lblDateReception.Name = "lblDateReception";
            this.lblDateReception.Size = new System.Drawing.Size(106, 15);
            this.lblDateReception.TabIndex = 5;
            this.lblDateReception.Text = "Date de reception :";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(227, 188);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(84, 33);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(317, 188);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(84, 33);
            this.btnAnnuler.TabIndex = 6;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // gboCategorieAliment
            // 
            this.gboCategorieAliment.Controls.Add(this.optDivers);
            this.gboCategorieAliment.Controls.Add(this.optOeuf);
            this.gboCategorieAliment.Controls.Add(this.optCerealier);
            this.gboCategorieAliment.Controls.Add(this.optLaitier);
            this.gboCategorieAliment.Controls.Add(this.optViandeEtSub);
            this.gboCategorieAliment.Controls.Add(this.optFruitEtLegume);
            this.gboCategorieAliment.Location = new System.Drawing.Point(338, 21);
            this.gboCategorieAliment.Name = "gboCategorieAliment";
            this.gboCategorieAliment.Size = new System.Drawing.Size(273, 117);
            this.gboCategorieAliment.TabIndex = 7;
            this.gboCategorieAliment.TabStop = false;
            this.gboCategorieAliment.Text = "Catégorie d\'aliment";
            // 
            // optDivers
            // 
            this.optDivers.AutoSize = true;
            this.optDivers.Location = new System.Drawing.Point(149, 87);
            this.optDivers.Name = "optDivers";
            this.optDivers.Size = new System.Drawing.Size(82, 19);
            this.optDivers.TabIndex = 0;
            this.optDivers.TabStop = true;
            this.optDivers.Text = "Divers secs";
            this.optDivers.UseVisualStyleBackColor = true;
            // 
            // optOeuf
            // 
            this.optOeuf.AutoSize = true;
            this.optOeuf.Location = new System.Drawing.Point(149, 62);
            this.optOeuf.Name = "optOeuf";
            this.optOeuf.Size = new System.Drawing.Size(56, 19);
            this.optOeuf.TabIndex = 0;
            this.optOeuf.TabStop = true;
            this.optOeuf.Text = "Oeufs";
            this.optOeuf.UseVisualStyleBackColor = true;
            // 
            // optCerealier
            // 
            this.optCerealier.AutoSize = true;
            this.optCerealier.Location = new System.Drawing.Point(149, 38);
            this.optCerealier.Name = "optCerealier";
            this.optCerealier.Size = new System.Drawing.Size(118, 19);
            this.optCerealier.TabIndex = 0;
            this.optCerealier.TabStop = true;
            this.optCerealier.Text = "Produit Céréaliers";
            this.optCerealier.UseVisualStyleBackColor = true;
            // 
            // optLaitier
            // 
            this.optLaitier.AutoSize = true;
            this.optLaitier.Location = new System.Drawing.Point(6, 87);
            this.optLaitier.Name = "optLaitier";
            this.optLaitier.Size = new System.Drawing.Size(99, 19);
            this.optLaitier.TabIndex = 0;
            this.optLaitier.TabStop = true;
            this.optLaitier.Text = "Produit Laitier";
            this.optLaitier.UseVisualStyleBackColor = true;
            // 
            // optViandeEtSub
            // 
            this.optViandeEtSub.AutoSize = true;
            this.optViandeEtSub.Location = new System.Drawing.Point(6, 62);
            this.optViandeEtSub.Name = "optViandeEtSub";
            this.optViandeEtSub.Size = new System.Drawing.Size(130, 19);
            this.optViandeEtSub.TabIndex = 0;
            this.optViandeEtSub.TabStop = true;
            this.optViandeEtSub.Text = "Viandes et Substitus";
            this.optViandeEtSub.UseVisualStyleBackColor = true;
            // 
            // optFruitEtLegume
            // 
            this.optFruitEtLegume.AutoSize = true;
            this.optFruitEtLegume.Location = new System.Drawing.Point(6, 37);
            this.optFruitEtLegume.Name = "optFruitEtLegume";
            this.optFruitEtLegume.Size = new System.Drawing.Size(118, 19);
            this.optFruitEtLegume.TabIndex = 0;
            this.optFruitEtLegume.TabStop = true;
            this.optFruitEtLegume.Text = "Fruits et Légumes";
            this.optFruitEtLegume.UseVisualStyleBackColor = true;
            // 
            // txtQttyAliment
            // 
            this.txtQttyAliment.Location = new System.Drawing.Point(227, 40);
            this.txtQttyAliment.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtQttyAliment.Name = "txtQttyAliment";
            this.txtQttyAliment.Size = new System.Drawing.Size(102, 23);
            this.txtQttyAliment.TabIndex = 8;
            // 
            // lblQttyAliment
            // 
            this.lblQttyAliment.AutoSize = true;
            this.lblQttyAliment.Location = new System.Drawing.Point(227, 22);
            this.lblQttyAliment.Name = "lblQttyAliment";
            this.lblQttyAliment.Size = new System.Drawing.Size(59, 15);
            this.lblQttyAliment.TabIndex = 9;
            this.lblQttyAliment.Text = "Quantité :";
            // 
            // cbBoxUniteMesure
            // 
            this.cbBoxUniteMesure.FormattingEnabled = true;
            this.cbBoxUniteMesure.Location = new System.Drawing.Point(227, 103);
            this.cbBoxUniteMesure.Name = "cbBoxUniteMesure";
            this.cbBoxUniteMesure.Size = new System.Drawing.Size(102, 23);
            this.cbBoxUniteMesure.TabIndex = 10;
            // 
            // lblUniteMesure
            // 
            this.lblUniteMesure.AutoSize = true;
            this.lblUniteMesure.Location = new System.Drawing.Point(227, 85);
            this.lblUniteMesure.Name = "lblUniteMesure";
            this.lblUniteMesure.Size = new System.Drawing.Size(99, 15);
            this.lblUniteMesure.TabIndex = 11;
            this.lblUniteMesure.Text = "Unité de mesure :";
            // 
            // FenetreAliment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 233);
            this.Controls.Add(this.lblUniteMesure);
            this.Controls.Add(this.cbBoxUniteMesure);
            this.Controls.Add(this.lblQttyAliment);
            this.Controls.Add(this.txtQttyAliment);
            this.Controls.Add(this.gboCategorieAliment);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblDateReception);
            this.Controls.Add(this.txtDateReception);
            this.Controls.Add(this.txtNomAliment);
            this.Controls.Add(this.lblNomAliment);
            this.Name = "FenetreAliment";
            this.ShowInTaskbar = false;
            this.Text = "FenetreAjoutModifAliment";
            this.gboCategorieAliment.ResumeLayout(false);
            this.gboCategorieAliment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQttyAliment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomAliment;
        private System.Windows.Forms.TextBox txtNomAliment;
        private System.Windows.Forms.TextBox txtDateReception;
        private System.Windows.Forms.Label lblDateReception;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.GroupBox gboCategorieAliment;
        private System.Windows.Forms.RadioButton optViandeEtSub;
        private System.Windows.Forms.RadioButton optFruitEtLegume;
        private System.Windows.Forms.RadioButton optDivers;
        private System.Windows.Forms.RadioButton optOeuf;
        private System.Windows.Forms.RadioButton optCerealier;
        private System.Windows.Forms.RadioButton optLaitier;
        private System.Windows.Forms.NumericUpDown txtQttyAliment;
        private System.Windows.Forms.Label lblQttyAliment;
        private System.Windows.Forms.ComboBox cbBoxUniteMesure;
        private System.Windows.Forms.Label lblUniteMesure;
    }
}