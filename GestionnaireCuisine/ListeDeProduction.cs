﻿using System.Collections.Generic;

namespace GestionnaireCuisine
{
    public class ListeDeProduction
    {
        #region Attributs

        private static List<(string, float, float, float, UniteMesure)> _listeRecetteAProduire;

        #endregion Attributs

        #region Accesseurs

        public List<(string, float, float, float, UniteMesure)> ListeRecetteAProduire
        {
            get { return ListeDeProduction._listeRecetteAProduire; }
            set { ListeDeProduction._listeRecetteAProduire = value; }
        }

        #endregion Accesseurs

        #region Constructeur

        public ListeDeProduction()
        {
            this.ListeRecetteAProduire = new List<(string, float, float, float, UniteMesure)>();
        }

        #endregion Constructeur

        #region Méthodes

        /// <summary>
        /// Retourne la quantité d'aliment à produire selon la disponibilité en inventaire
        /// </summary>
        /// <param name="qttyAlimentNecessaire">Quantité d'aliment nécessaire à produire</param>
        /// <param name="qttyAlimentDisponible">Quantité disponible en inventaire</param>
        /// <returns>QUantité d'aliment à produire selon la disponibilité en inventaire</returns>
        private static float CalculerQuantiteACommander(float qttyAlimentNecessaire, float qttyAlimentDisponible)
        {
            return qttyAlimentNecessaire - qttyAlimentDisponible;
        }



        /// <summary>
        /// Méthode permettant de générer la liste d'aliment à produire selon la recette
        /// et sa quantité à produire ainsi que l'inventaire
        /// </summary>
        /// <param name="recette">Recette à produire</param>
        /// <param name="inventaire">Inventaire</param>
        /// <param name="pQuantiteRecetteNecessaire">Quantité de recette à produire</param>
        public static void GenererListeAlimentAProduire(Recette recette, Inventaire inventaire,
            int pQuantiteRecetteNecessaire)
        {
            foreach ((Aliment, float, UniteMesure) tupleAlimentQtty in recette.LstAlimentsQtty)
            {
                Stock stock = inventaire.GetStockSelonAliment(tupleAlimentQtty.Item1);
                float qttyAlimentNecessaire = tupleAlimentQtty.Item2 * pQuantiteRecetteNecessaire;
                float qttyAlimentDisponible = 0;
                float qttyAProduire;
                float qttyDispoApresProd;
                float qttyACommander;
                if (tupleAlimentQtty.Item3 == stock.UniteMesure)
                {
                    qttyAlimentDisponible = stock.Quantite;
                }
                else
                {
                    switch (tupleAlimentQtty.Item3)
                    {
                        case UniteMesure.Gramme:
                        case UniteMesure.Millilitre:
                            qttyAlimentDisponible = stock.Quantite * 1000;
                            break;
                        case UniteMesure.Kilogramme:
                        case UniteMesure.Litre:
                            qttyAlimentDisponible = stock.Quantite / 1000;
                            break;
                    }
                }

                if (qttyAlimentNecessaire > qttyAlimentDisponible)
                {
                    qttyAProduire = qttyAlimentDisponible;
                    qttyDispoApresProd = 0;
                    qttyACommander = CalculerQuantiteACommander(qttyAlimentNecessaire, qttyAlimentDisponible);
                }
                else
                {
                    qttyAProduire = qttyAlimentNecessaire;
                    qttyDispoApresProd = qttyAlimentDisponible - qttyAProduire;
                    qttyACommander = 0;
                }

                bool bEstDejaPresent = false;
                int iIndexAliment = 0;
                if (_listeRecetteAProduire.Count != 0)
                {
                    for (int i = 0; i < _listeRecetteAProduire.Count; i++)
                    {
                        if (_listeRecetteAProduire[i].Item1 == tupleAlimentQtty.Item1.Nom)
                        {
                            bEstDejaPresent = true;
                            iIndexAliment = i;
                        }
                    }

                    if (bEstDejaPresent)
                    {
                        float nouvQttyProduire = 0;
                        float nouvQttyCommander = 0;
                        float nouvQttyDispoApres = 0;
                        if (_listeRecetteAProduire[iIndexAliment].Item5 == tupleAlimentQtty.Item3)
                        {
                            nouvQttyProduire = _listeRecetteAProduire[iIndexAliment].Item2 + qttyAProduire;
                            nouvQttyCommander = _listeRecetteAProduire[iIndexAliment].Item4 + qttyACommander;
                            nouvQttyDispoApres = qttyDispoApresProd;
                        }
                        else
                        {
                            switch (tupleAlimentQtty.Item3)
                            {
                                case UniteMesure.Gramme:
                                case UniteMesure.Millilitre:
                                    nouvQttyProduire = _listeRecetteAProduire[iIndexAliment].Item2 + qttyAProduire / 1000;
                                    nouvQttyCommander = _listeRecetteAProduire[iIndexAliment].Item4 + qttyACommander / 1000;
                                    nouvQttyDispoApres = qttyDispoApresProd / 1000;
                                    break;
                                case UniteMesure.Kilogramme:
                                case UniteMesure.Litre:
                                    nouvQttyProduire = _listeRecetteAProduire[iIndexAliment].Item2 + qttyAProduire * 1000;
                                    nouvQttyCommander = _listeRecetteAProduire[iIndexAliment].Item4 + qttyACommander * 1000;
                                    nouvQttyDispoApres = qttyDispoApresProd * 1000;
                                    break;
                            }
                        }

                        UniteMesure uniteMesure = _listeRecetteAProduire[iIndexAliment].Item5;
                        _listeRecetteAProduire.RemoveAt(iIndexAliment);
                        _listeRecetteAProduire.Add((tupleAlimentQtty.Item1.Nom, nouvQttyProduire, nouvQttyDispoApres,
                            nouvQttyCommander, uniteMesure));
                    }
                    else
                    {
                        _listeRecetteAProduire.Add((tupleAlimentQtty.Item1.Nom, qttyAProduire, qttyDispoApresProd,
                            qttyACommander, tupleAlimentQtty.Item3));
                    }
                }
                else
                {
                    _listeRecetteAProduire.Add((tupleAlimentQtty.Item1.Nom, qttyAProduire, qttyDispoApresProd,
                        qttyACommander, tupleAlimentQtty.Item3));
                }
                if (tupleAlimentQtty.Item3 == stock.UniteMesure)
                {
                    stock.Quantite = qttyDispoApresProd;
                }
                else
                {
                    switch (tupleAlimentQtty.Item3)
                    {
                        case UniteMesure.Gramme:
                        case UniteMesure.Millilitre:
                            stock.Quantite = qttyDispoApresProd / 1000;
                            break;
                        case UniteMesure.Kilogramme:
                        case UniteMesure.Litre:
                            stock.Quantite = qttyDispoApresProd * 1000;
                            break;
                    }
                }
            }
        }

        #endregion Méthodes
    }
}