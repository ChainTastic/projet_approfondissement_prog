﻿using System;
using System.Collections.Generic;

namespace GestionnaireCuisine
{
    public class LivreRecette
    {
        #region Attributs

        private List<Recette> _listeRecettes;

        #endregion Attributs

        #region Accesseurs

        public List<Recette> ListeRecettes
        {
            get { return _listeRecettes; }
            set { _listeRecettes = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public LivreRecette()
        {
            _listeRecettes = new List<Recette>();
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode permettant d'ajouter une recette à la liste de recette
        /// </summary>
        /// <param name="uneRecette">objet de classe Recette</param>
        /// <returns></returns>
        public bool AjouterRecette(Recette uneRecette)
        {
            if (_listeRecettes.Contains(uneRecette) || uneRecette == null)
            {
                return false;
            }

            _listeRecettes.Add(uneRecette);
            return true;
        }

        /// <summary>
        /// Méthode permettant de retirer une recette de la liste de recettes
        /// </summary>
        /// <param name="uneRecette">objet de classe Recett</param>
        /// <returns></returns>
        public bool RetirerRecette(Recette uneRecette)
        {
            return _listeRecettes.Remove(uneRecette);
        }

        /// <summary>
        /// Méthode permettant d'obtenir une recette du Livre de recette à l'indice donnée en paramètre
        /// </summary>
        /// <param name="pIndiceRecette">Indice de la recette dans la liste de recette</param>
        /// <returns>Recette dans la liste de recettes à l'indice donnée</returns>
        public Recette ObtenirRecette(int pIndiceRecette)
        {
            if (pIndiceRecette >= _listeRecettes.Count || pIndiceRecette < 0)
            {
                throw new IndexOutOfRangeException("L'indice est en dehors de la plage.");
            }

            return _listeRecettes[pIndiceRecette];
        }

        /// <summary>
        /// Méthode permettant d'obtenir une recette du Livre de recette selon le nom de la recette en paramètre
        /// </summary>
        /// <param name="nomRecette">chaîne de caractère représentant le nom de la recette</param>
        /// <returns>Objet Recette ayant comme propriété Nom la chaîne de caractère envoyé en paramètre</returns>
        public Recette ObtenirRecetteSelonNom(string nomRecette)
        {
            foreach (Recette recette in this._listeRecettes)
            {
                if (recette.Nom.ToLower() == nomRecette.ToLower())
                {
                    return recette;
                }
            }

            throw new ArgumentException("La recette envoyé en paramètre n'existe pas");
        }

        /// <summary>
        /// Permet d'obtenir toutes les recettes ayant comme aliment la chaîne de caractères recherchée par l'utilisateur
        /// </summary>
        /// <param name="pThermeRecherche">chaîne de caractères recherchée par l'utilisateur</param>
        /// <returns>Liste de recette ayant comme aliment la chaîne de caractères recherchée par l'utilisateur</returns>
        public List<Recette> ObtenirRecettesSelonAliment(string pThermeRecherche)
        {
            List<Recette> listeResultats = new List<Recette>();
            foreach (Recette recette in ListeRecettes)
            {
                if (recette.ContientAliment(pThermeRecherche))
                {
                    listeResultats.Add(recette);
                }
            }

            return listeResultats;
        }

        #endregion Méthodes
        /// <summary>
        /// Permet d'obtenir toutes les recette dont le nom contient la chaîne de caractères recherché par l'utilisateur
        /// </summary>
        /// <param name="pThermeRecherche">chaîne de caractères recherché par l'utilisateur</param>
        /// <returns>Liste de recette dont le nom contient la chaîne de caractère recherché par l'utilisateur</returns>
        public List<Recette> ObtenirRecettesSelonNom(string pThermeRecherche)
        {
            List<Recette> listeResultats = new List<Recette>();
            foreach (Recette recette in ListeRecettes)
            {
                if (recette.ContientNom(pThermeRecherche))
                {
                    listeResultats.Add(recette);
                }
            }

            return listeResultats;
        }
    }
}