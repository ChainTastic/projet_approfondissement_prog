﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace GestionnaireCuisine
{
    public partial class FenetreListeProduction : Form
    {
        #region Attributs

        private Inventaire _lInventaire;
        private LivreRecette _leLivreDeRecette;
        private ListeDeProduction _uneListeDeProduction;

        #endregion Attributs

        #region Accesseurs

        public ListeDeProduction UneListeDeProduction
        {
            get { return _uneListeDeProduction; }
            set { _uneListeDeProduction = value; }
        }

        #endregion Accesseurs

        #region Constructeur

        public FenetreListeProduction()
        {
            InitializeComponent();
            FenetrePrincipal.LeGestionnaire.ChargerXml();
            if (File.Exists(Gestionnaire.CheminListeProduction))
            {
                _uneListeDeProduction = FenetrePrincipal.LeGestionnaire.ListeDeProduction;
            }
            else
            {
                _uneListeDeProduction = new ListeDeProduction();
            }

            _lInventaire = FenetrePrincipal.LeGestionnaire.Inventaire;
            _leLivreDeRecette = FenetrePrincipal.LeGestionnaire.LivreDeRecette;
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = btnOK;
            CancelButton = btnAnnuler;
            btnOK.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
            colChoixRecette.DataSource = _leLivreDeRecette.ListeRecettes;
            colChoixRecette.ValueMember = "Nom";
            colChoixRecette.DisplayMember = colChoixRecette.ValueMember;
            colChoixRecette.ValueType = typeof(Recette);
            colQtty.ValueType = typeof(int);
        }

        #endregion Constructeur

        #region Événements

        private void FenetreListeProduction_Load(object sender, EventArgs e)
        {
            RemplirGrilleProduction(_uneListeDeProduction.ListeRecetteAProduire);
        }

        private void btnProduireListeProd_Click(object sender, EventArgs e)
        {
            if (GrilleRecetteEstValide())
            {
                if (_uneListeDeProduction.ListeRecetteAProduire.Count > 0)
                {
                    grilleProduction.Rows.Clear();
                    RemplirGrilleProduction(_uneListeDeProduction.ListeRecetteAProduire);
                   
                }
                else
                {
                    MessageBox.Show("Aucun aliment à produire");
                }
            }
        }
        

        private void btnExporterPDF_Click(object sender, EventArgs e)
        {
            if (grilleProduction.Rows.Count > 0)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "PDF (*.pdf) |*.pdf";
                sfd.DefaultExt = "pdf";
                sfd.AddExtension = true;

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    Gestionnaire.ExportationPdf(sfd.FileName, grilleProduction);
                }
            }
            else
            {
                MessageBox.Show("Aucune liste de production à exporter !!", "Info");
            }
        }

        private void btnReinitialiserListeProd_Click(object sender, EventArgs e)
        {
            if (grilleProduction.Rows.Count > 0)
            {
                grilleProduction.Rows.Clear();
                FenetrePrincipal.LeGestionnaire.ListeDeProduction = new ListeDeProduction();
            }
        }


        #endregion Événements

        #region Méthodes

        /// <summary>
        /// Méthode permettant de vérifier si la grille de Recette est valide
        /// </summary>
        /// <returns>Retourne vrai si l'ensemble des recettes à produire sont valides, faux autrement</returns>
        private bool GrilleRecetteEstValide()
        {
            if (!grilleRecette.Rows[0].IsNewRow)
            {
                foreach (DataGridViewRow grilleRecetteRow in grilleRecette.Rows)
                {
                    if (!ValiderRecetteAProduire(grilleRecetteRow))
                    {
                        return false;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "Vous devez ajouter une recette à produire avant de pouvoir produire une liste de production");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Méthode permettant de valider la recette a produire
        /// </summary>
        /// <param name="grilleRecetteRow">Objet DataGridViewRow représentant une recette</param>
        /// <returns>Vrai si la recette peut être produite, Faux autrement</returns>
        private bool ValiderRecetteAProduire(DataGridViewRow grilleRecetteRow)
        {
            if (!grilleRecetteRow.IsNewRow)
            {
                if (VerifierRow(grilleRecetteRow))
                {
                    Recette recette =
                        _leLivreDeRecette.ObtenirRecetteSelonNom(grilleRecetteRow.Cells[0].Value.ToString());
                    int iQuantiteRecette = Convert.ToInt32(grilleRecetteRow.Cells[1].Value);
                    if (recette.VerifierDisponibiliteIngredientsEnInventaire(_lInventaire))
                    {
                        ListeDeProduction.GenererListeAlimentAProduire(recette, _lInventaire, iQuantiteRecette);
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Méthode permettant de vérifier chaque champ de la rangée reçue en paramètre
        /// </summary>
        /// <param name="grilleRecetteRow">Objet DataGridViewRow représentant une recette</param>
        /// <returns>Vrai si il est valide, Faux autrement</returns>
        private bool VerifierRow(DataGridViewRow grilleRecetteRow)
        {
            if (grilleRecetteRow.State == DataGridViewElementStates.None)
            {
                MessageBox.Show(
                    "Vous devez ajouter une recette à produire pour pouvoir produire une liste de production");
                return false;
            }

            if (grilleRecetteRow.Cells[0].Value is null)
            {
                MessageBox.Show(
                    "Vous devez sélectionner une recette à produire avant de pouvoir produire une liste de production");
                return false;
            }

            if (grilleRecetteRow.Cells[1].Value is null)
            {
                MessageBox.Show("Vous devez entrer une quantité pour chaque recette à produire");
                return false;
            }

            if (!FenetrePrincipal.LeGestionnaire.EstUnNombreValide(grilleRecetteRow.Cells[1].Value))
            {
                MessageBox.Show("La quantité de recette à produire doit être composé uniquement de chiffre");
                return false;
            }

            return true;
        }



        /// <summary>
        /// Permet de remplir la gridview selon la liste de production
        /// </summary>
        /// <param name="pListeProduction">liste de production</param>
        private void RemplirGrilleProduction(List<(string, float, float, float, UniteMesure)> pListeProduction)
        {
            foreach ((string, float, float, float, UniteMesure) alimentAProduire in pListeProduction)
            {
                (float, UniteMesure) qttyProduire = FenetrePrincipal.LeGestionnaire.ObtenirQuantiteOptimale(alimentAProduire.Item2, alimentAProduire.Item5);
                (float, UniteMesure) qttyDispo = FenetrePrincipal.LeGestionnaire.ObtenirQuantiteOptimale(alimentAProduire.Item3, alimentAProduire.Item5);
                (float, UniteMesure) qttyCommande = FenetrePrincipal.LeGestionnaire.ObtenirQuantiteOptimale(alimentAProduire.Item4, alimentAProduire.Item5);
                grilleProduction.Rows.Add(alimentAProduire.Item1,
                    qttyProduire.Item1 + " " + FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(qttyProduire.Item2),
                    qttyDispo.Item1 + " " + FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(qttyDispo.Item2),
                    qttyCommande.Item1 + " " + FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(qttyCommande.Item2)
                );
            }
        }


        #endregion Méthodes
    }
}