﻿namespace GestionnaireCuisine
{
    partial class l
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreerRecette = new System.Windows.Forms.Button();
            this.btnModifierRecette = new System.Windows.Forms.Button();
            this.btnSupprimerRecette = new System.Windows.Forms.Button();
            this.lstVwLivreRecette = new System.Windows.Forms.ListView();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.lblListeIngredientsRecette = new System.Windows.Forms.Label();
            this.lblListeRecettes = new System.Windows.Forms.Label();
            this.txtFiltreRecetteSelonAliment = new System.Windows.Forms.TextBox();
            this.teSelonAliment = new System.Windows.Forms.Label();
            this.lstVwIngredients = new System.Windows.Forms.ListView();
            this.colIngredientRecette = new System.Windows.Forms.ColumnHeader();
            this.colIngredientQtty = new System.Windows.Forms.ColumnHeader();
            this.txtFiltreSelonNomRecette = new System.Windows.Forms.TextBox();
            this.lblFiltreSelonNomRecette = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreerRecette
            // 
            this.btnCreerRecette.Location = new System.Drawing.Point(217, 191);
            this.btnCreerRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCreerRecette.Name = "btnCreerRecette";
            this.btnCreerRecette.Size = new System.Drawing.Size(89, 39);
            this.btnCreerRecette.TabIndex = 0;
            this.btnCreerRecette.Text = "Créer une recette";
            this.btnCreerRecette.UseVisualStyleBackColor = true;
            this.btnCreerRecette.Click += new System.EventHandler(this.btnCreerRecette_Click);
            // 
            // btnModifierRecette
            // 
            this.btnModifierRecette.Location = new System.Drawing.Point(217, 269);
            this.btnModifierRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnModifierRecette.Name = "btnModifierRecette";
            this.btnModifierRecette.Size = new System.Drawing.Size(89, 43);
            this.btnModifierRecette.TabIndex = 0;
            this.btnModifierRecette.Text = "Modifier la recette";
            this.btnModifierRecette.UseVisualStyleBackColor = true;
            this.btnModifierRecette.Click += new System.EventHandler(this.btnModifierRecette_Click);
            // 
            // btnSupprimerRecette
            // 
            this.btnSupprimerRecette.Location = new System.Drawing.Point(217, 361);
            this.btnSupprimerRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSupprimerRecette.Name = "btnSupprimerRecette";
            this.btnSupprimerRecette.Size = new System.Drawing.Size(89, 42);
            this.btnSupprimerRecette.TabIndex = 0;
            this.btnSupprimerRecette.Text = "Supprimer la recette";
            this.btnSupprimerRecette.UseVisualStyleBackColor = true;
            this.btnSupprimerRecette.Click += new System.EventHandler(this.btnSupprimerRecette_Click);
            // 
            // lstVwLivreRecette
            // 
            this.lstVwLivreRecette.HideSelection = false;
            this.lstVwLivreRecette.Location = new System.Drawing.Point(13, 135);
            this.lstVwLivreRecette.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.lstVwLivreRecette.Name = "lstVwLivreRecette";
            this.lstVwLivreRecette.Size = new System.Drawing.Size(167, 319);
            this.lstVwLivreRecette.TabIndex = 3;
            this.lstVwLivreRecette.UseCompatibleStateImageBehavior = false;
            this.lstVwLivreRecette.SelectedIndexChanged += new System.EventHandler(this.lstVwLivreRecette_SelectedIndexChanged);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(437, 480);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(91, 33);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(339, 480);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(91, 33);
            this.btnAnnuler.TabIndex = 6;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // lblListeIngredientsRecette
            // 
            this.lblListeIngredientsRecette.AutoSize = true;
            this.lblListeIngredientsRecette.Location = new System.Drawing.Point(361, 101);
            this.lblListeIngredientsRecette.MaximumSize = new System.Drawing.Size(145, 0);
            this.lblListeIngredientsRecette.Name = "lblListeIngredientsRecette";
            this.lblListeIngredientsRecette.Size = new System.Drawing.Size(69, 15);
            this.lblListeIngredientsRecette.TabIndex = 7;
            this.lblListeIngredientsRecette.Text = "placeholder";
            this.lblListeIngredientsRecette.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblListeIngredientsRecette.Visible = false;
            // 
            // lblListeRecettes
            // 
            this.lblListeRecettes.AutoSize = true;
            this.lblListeRecettes.Location = new System.Drawing.Point(12, 117);
            this.lblListeRecettes.Name = "lblListeRecettes";
            this.lblListeRecettes.Size = new System.Drawing.Size(96, 15);
            this.lblListeRecettes.TabIndex = 8;
            this.lblListeRecettes.Text = "Liste des recettes";
            // 
            // txtFiltreRecetteSelonAliment
            // 
            this.txtFiltreRecetteSelonAliment.Location = new System.Drawing.Point(200, 77);
            this.txtFiltreRecetteSelonAliment.Name = "txtFiltreRecetteSelonAliment";
            this.txtFiltreRecetteSelonAliment.Size = new System.Drawing.Size(133, 23);
            this.txtFiltreRecetteSelonAliment.TabIndex = 9;
            this.txtFiltreRecetteSelonAliment.TextChanged += new System.EventHandler(this.txtFiltreRecetteSelonAliment_TextChanged);
            // 
            // teSelonAliment
            // 
            this.teSelonAliment.AutoSize = true;
            this.teSelonAliment.Location = new System.Drawing.Point(12, 80);
            this.teSelonAliment.Name = "teSelonAliment";
            this.teSelonAliment.Size = new System.Drawing.Size(120, 15);
            this.teSelonAliment.TabIndex = 10;
            this.teSelonAliment.Text = "Filtrer selon aliment : ";
            // 
            // lstVwIngredients
            // 
            this.lstVwIngredients.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIngredientRecette,
            this.colIngredientQtty});
            this.lstVwIngredients.HideSelection = false;
            this.lstVwIngredients.Location = new System.Drawing.Point(339, 135);
            this.lstVwIngredients.Name = "lstVwIngredients";
            this.lstVwIngredients.Size = new System.Drawing.Size(220, 319);
            this.lstVwIngredients.TabIndex = 11;
            this.lstVwIngredients.UseCompatibleStateImageBehavior = false;
            this.lstVwIngredients.View = System.Windows.Forms.View.Details;
            // 
            // colIngredientRecette
            // 
            this.colIngredientRecette.Text = "Nom";
            this.colIngredientRecette.Width = 135;
            // 
            // colIngredientQtty
            // 
            this.colIngredientQtty.Text = "Quantité";
            this.colIngredientQtty.Width = 85;
            // 
            // txtFiltreSelonNomRecette
            // 
            this.txtFiltreSelonNomRecette.Location = new System.Drawing.Point(199, 29);
            this.txtFiltreSelonNomRecette.Name = "txtFiltreSelonNomRecette";
            this.txtFiltreSelonNomRecette.Size = new System.Drawing.Size(134, 23);
            this.txtFiltreSelonNomRecette.TabIndex = 12;
            this.txtFiltreSelonNomRecette.TextChanged += new System.EventHandler(this.txtFiltreSelonNomRecette_TextChanged);
            // 
            // lblFiltreSelonNomRecette
            // 
            this.lblFiltreSelonNomRecette.AutoSize = true;
            this.lblFiltreSelonNomRecette.Location = new System.Drawing.Point(12, 32);
            this.lblFiltreSelonNomRecette.Name = "lblFiltreSelonNomRecette";
            this.lblFiltreSelonNomRecette.Size = new System.Drawing.Size(181, 15);
            this.lblFiltreSelonNomRecette.TabIndex = 13;
            this.lblFiltreSelonNomRecette.Text = "Filtrer selon le nom de la recette :";
            // 
            // l
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 525);
            this.Controls.Add(this.lblFiltreSelonNomRecette);
            this.Controls.Add(this.txtFiltreSelonNomRecette);
            this.Controls.Add(this.lstVwIngredients);
            this.Controls.Add(this.teSelonAliment);
            this.Controls.Add(this.txtFiltreRecetteSelonAliment);
            this.Controls.Add(this.lblListeRecettes);
            this.Controls.Add(this.lblListeIngredientsRecette);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lstVwLivreRecette);
            this.Controls.Add(this.btnSupprimerRecette);
            this.Controls.Add(this.btnModifierRecette);
            this.Controls.Add(this.btnCreerRecette);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "l";
            this.Text = "Livre de Recettes";
            this.Load += new System.EventHandler(this.FenetreLivreRecettes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button btnModifierRecette;

        #endregion

        private System.Windows.Forms.Button btnCreerRecette;
        private System.Windows.Forms.Button btnSupprimerRecette;
        private System.Windows.Forms.ListView lstVwLivreRecette;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label lblListeIngredientsRecette;
        private System.Windows.Forms.Label lblListeRecettes;
        private System.Windows.Forms.TextBox txtFiltreRecetteSelonAliment;
        private System.Windows.Forms.Label teSelonAliment;
        private System.Windows.Forms.ColumnHeader colIngredientRecette;
        private System.Windows.Forms.ColumnHeader colIngredientQtty;
        private System.Windows.Forms.ListView lstVwIngredients;
        private System.Windows.Forms.TextBox txtFiltreSelonNomRecette;
        private System.Windows.Forms.Label lblFiltreSelonNomRecette;
    }
}