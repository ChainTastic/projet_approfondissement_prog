﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using iText.IO.Font.Constants;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout.Element;
using iText.Layout.Properties;
using Document = iText.Layout.Document;


namespace GestionnaireCuisine
{
    public class Gestionnaire
    {
        #region Attributs

        private static LivreRecette _livreDeRecette;
        private static Inventaire _inventaire;
        private static ListeDeProduction _listeDeProduction;
        private const string Cheminfichierdata = "..//..//..//Data//";
        public const string CheminInventaire = Gestionnaire.Cheminfichierdata + "inventaire.xml";
        public const string CheminLivreRecette = Gestionnaire.Cheminfichierdata + "livreDeRecette.xml";
        public const string CheminListeProduction = Gestionnaire.Cheminfichierdata + "listeDeProduction.xml";

        #endregion Attributs

        #region Accesseurs

        public LivreRecette LivreDeRecette
        {
            get { return Gestionnaire._livreDeRecette; }
            set { Gestionnaire._livreDeRecette = value; }
        }

        public Inventaire Inventaire
        {
            get { return Gestionnaire._inventaire; }
            set { Gestionnaire._inventaire = value; }
        }

        public ListeDeProduction ListeDeProduction
        {
            get { return Gestionnaire._listeDeProduction; }
            set { Gestionnaire._listeDeProduction = value; }
        }

        #endregion Accesseurs

        #region Méthodes

        /// <summary>
        /// Méthode permettant de générer un fichier XML selon les paramètres donnés
        /// </summary>
        /// <param name="pType">type de l'objet XML à produire</param>
        /// <param name="pObjectASerialize">Objet à Sérialiser en fichier XML</param>
        /// <param name="pNomDuFichier">Nom du fichier XML à produire sans l'extension</param>
        public void GenererXml(Type pType, object pObjectASerialize, string pNomDuFichier)
        {
            XmlSerializer writerXml = new XmlSerializer(pType);
            using (FileStream dataFile =
                File.Create(string.Format("{0}{1}.xml", Gestionnaire.Cheminfichierdata, pNomDuFichier)))
            {
                writerXml.Serialize(dataFile, pObjectASerialize);
            }
        }

        /// <summary>
        /// Méthode permettant d'affecter les données des fichiers XML existant à leur objet correspondant
        /// </summary>
        public void ChargerXml()
        {
            if (File.Exists(Gestionnaire.CheminInventaire))
            {
                XmlSerializer readerInventaire = new XmlSerializer(typeof(Inventaire));
                using (StreamReader sr = new StreamReader(Gestionnaire.CheminInventaire))
                {
                    Inventaire = (Inventaire) readerInventaire.Deserialize(sr);
                }
            }
            else
            {
                Inventaire = new Inventaire();
            }

            if (File.Exists(Gestionnaire.CheminLivreRecette))
            {
                XmlSerializer readerLivreRecette = new XmlSerializer(typeof(LivreRecette));
                using (StreamReader sr = new StreamReader(Gestionnaire.CheminLivreRecette))
                {
                    LivreDeRecette = (LivreRecette) readerLivreRecette.Deserialize(sr);
                }
            }
            else
            {
                LivreDeRecette = new LivreRecette();
            }

            if (File.Exists(Gestionnaire.CheminListeProduction))
            {
                XmlSerializer readerListeProduction = new XmlSerializer(typeof(ListeDeProduction));
                using (StreamReader sr = new StreamReader(Gestionnaire.CheminListeProduction))
                {
                    ListeDeProduction = (ListeDeProduction) readerListeProduction.Deserialize(sr);
                }
            }
            else
            {
                ListeDeProduction = new ListeDeProduction();
            }
        }

        /// <summary>
        /// Permet d'exporter la liste de production (DataGridView) en fichier PDF
        /// </summary>
        /// <param name="pNomFichier">nom du fichier pdf choisie par l'utilisateur</param>
        /// <param name="pGrille">DataGridView représentant la liste d'aliment à produire</param>
        public static void ExportationPdf(string pNomFichier, DataGridView pGrille)
        {
            Table pdfTable = new Table(pGrille.Columns.Count);
            pdfTable.SetWidth(UnitValue.CreatePercentValue(100));


            foreach (DataGridViewColumn column in pGrille.Columns)
            {
                GenererHeader(column, pdfTable);
            }

            foreach (DataGridViewRow row in pGrille.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    Cell ligne = new Cell(1, 1);
                    Paragraph texte = new Paragraph(cell.Value.ToString());
                    ligne.Add(texte);
                    ligne.SetWidth(100);
                    pdfTable.AddCell(ligne);
                }
            }

            using (PdfWriter pwriter = new PdfWriter(pNomFichier))
            {
                PdfDocument pdfDoc = new PdfDocument(pwriter);
                Document doc = new Document(pdfDoc);
                Paragraph header = new Paragraph("Liste de production")
                    .SetTextAlignment(TextAlignment.CENTER)
                    .SetFontSize(20);
                // New line
                Paragraph newline = new Paragraph(new Text("\n"));

                doc.Add(newline);
                doc.Add(header);
                Paragraph subheader =
                    new Paragraph(DateTime.Now.ToString("ddd dd MMM yyyy", new CultureInfo("fr-CA", true)))
                        .SetTextAlignment(TextAlignment.CENTER)
                        .SetFontSize(15);

                doc.Add(subheader);
                doc.Add(newline);
                doc.Add(pdfTable);
                pdfDoc.Close();
                pwriter.Close();
            }

            MessageBox.Show("Liste de production exportée avec succès !!!", "Info");
        }

        /// <summary>
        /// Permet de generer un header pdf selon la colonne du DataGridView en paramètre
        /// </summary>
        /// <param name="column">colonne du DataGridView</param>
        /// <param name="pdfTable">Tableau PDF</param>
        private static void GenererHeader(DataGridViewColumn column, Table pdfTable)
        {
            PdfFont font = PdfFontFactory.CreateFont(StandardFonts.TIMES_BOLD);
            Text header = new Text(column.HeaderText)
                .SetFont(font);
            Cell cellule = new Cell(1, 1)
                .SetTextAlignment(TextAlignment.CENTER)
                .Add(new Paragraph(header));
            pdfTable.AddCell(cellule);
        }

        /// <summary>
        /// Permet d'obtenir la quantité optimale selon l'unité de mesure
        /// </summary>
        public (float, UniteMesure) ObtenirQuantiteOptimale(float pQuantite, UniteMesure pUniteMesure)
        {
            if (pQuantite >= 1000)
            {
                pQuantite /= 1000;
                if (pUniteMesure == UniteMesure.Gramme)
                {
                    pUniteMesure = UniteMesure.Kilogramme;
                }
                else if (pUniteMesure == UniteMesure.Millilitre)
                {
                    pUniteMesure = UniteMesure.Litre;
                }
            }
            return (pQuantite, pUniteMesure);
        }

        /// <summary>
        /// Permet d'obtenir une chaîne de caractère représentant le sigle de l'unité de mesure
        /// </summary>
        /// <returns>chaîne de caractère représentant le sigle de l'unité de mesure du stock</returns>
        public string ObtenirUniteMesureFormatte(UniteMesure pUniteMesure)
        {
            switch (pUniteMesure)
            {
                case UniteMesure.Gramme:
                    return "g";
                case UniteMesure.Kilogramme:
                    return "kg";
                case UniteMesure.Litre:
                    return "L";
                case UniteMesure.Livre:
                    return "Lbs";
                case UniteMesure.Millilitre:
                    return "ml";
                case UniteMesure.Once:
                    return "oz";
                case UniteMesure.Unité:
                    return "Unité(s)";
                default:
                    return "g";
            }
        }

        #endregion Méthodes
        /// <summary>
        /// Permet d'obtenir l'unité de mesure associé au sigle reçue en paramètre
        /// </summary>
        /// <param name="pSigle">chaîne de caractère représentant le sigle</param>
        /// <returns>Unité de mesure représenté par le sigle reçue en paramètre</returns>
        public UniteMesure ObtenirUniteSelonSigle(string pSigle)
        {
            switch (pSigle)
            {
                case "g":
                    return UniteMesure.Gramme;
                case "kg":
                    return UniteMesure.Kilogramme;
                case "L":
                    return UniteMesure.Litre;
                case "Lbs":
                    return UniteMesure.Livre;
                case "ml":
                    return UniteMesure.Millilitre;
                case "oz":
                    return UniteMesure.Once;
                case "Unité(s)":
                    return UniteMesure.Unité;
                default:
                    return UniteMesure.Gramme;

            }
        }

        /// <summary>
        /// Méthode permettant de vérifier si l'objet reçue en paramètre contient que des chiffres
        /// </summary>
        /// <param name="pQuantiteRecette">objet représentant la quantité de recette à produire</param>
        /// <returns>Retourne vrai si l'objet reçu en paramètre ne contient que des chiffres, faux autrement</returns>
        public bool EstUnNombreValide(object pQuantiteRecette)
        {
            if (pQuantiteRecette != null)

                foreach (char c in pQuantiteRecette.ToString())
                {
                    if (c < '0' || c > '9')
                    {
                        return false;
                    }
                }

            return true;
        }
    }
}