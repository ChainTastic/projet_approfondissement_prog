﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace GestionnaireCuisine
{
    public class Inventaire
    {
        #region Attributs

        private List<Stock> _lstStock;

        #endregion Attributs

        #region Accesseurs

        public List<Stock> LstStock
        {
            get { return _lstStock; }
            set { _lstStock = value; }
        }

        #endregion Accesseurs

        #region Constructeur

        public Inventaire()
        {
            _lstStock = new List<Stock>();
        }

        #endregion Constructeur

        #region Méthodes

        /// <summary>
        /// Permet d'obtenir le nombre de stocks qui se
        /// trouvent dans l'inventaire.
        /// </summary>
        public int GetNbStocks
        {
            get { return _lstStock.Count; }
        }

        /// <summary>
        /// retourne le stock qui se trouve à l'indice reçu en paramètre
        /// </summary>
        /// <param name="iIndice">indice du stock dans l'inventaire</param>
        /// <returns>
        ///le stock qui se trouve à l'indice reçu en paramètre dans
        /// m_lesStocks si cet indice est valide.
        /// </returns>
        public Stock GetStock(int iIndice)
        {
            if (iIndice >= _lstStock.Count || iIndice < 0)
            {
                throw new IndexOutOfRangeException(
                    "L'indice est en dehors de la plage.");
            }

            return _lstStock[iIndice];
        }

        /// <summary>
        /// retourne le stock de l'aliment reçu en paramètre
        /// </summary>
        /// <param name="pAliment">nom de l'aliment dans l'inventaire</param>
        /// <returns>
        ///le stock qui se trouve à l'indice reçu en paramètre dans
        /// m_lesStocks si cet indice est valide.
        /// </returns>
        public Stock GetStockSelonAliment(Aliment pAliment)
        {
            foreach (Stock stock in this._lstStock)
            {
                if (stock.Aliment.Nom == pAliment.Nom)
                {
                    return stock;
                }
            }

            throw new ArgumentException("L'aliment n'existe pas dans l'inventaire");
        }

        /// <summary>
        /// permet d'ajouter le stock reçu en paramètre à
        /// m_lesStocks s'il ne se trouve pas déjà dans la l'inventaire
        /// </summary>
        /// <param name="leStock"></param>
        /// <returns>
        /// vrai pour indiquer que le processus a réussi.Dans le cas où
        /// le stock reçu serait déjà dans l'inventaire,
        /// la méthode retourne faux.
        /// </returns>
        public bool AjouterStock(Stock leStock)
        {
            if (_lstStock.Contains(leStock) || leStock == null)
            {
                return false;
            }

            _lstStock.Add(leStock);
            return true;
        }

        /// <summary>
        /// permet de retirer le stock reçu en paramètre de m_lesStocks
        /// </summary>
        /// <param name="leStock">Le stock de l'aliment à retiré de l'inventaire</param>
        /// <returns>
        /// vrai si le stock à été retirer avec succès de l'inventaire, faux autrement
        /// </returns>
        public bool RetirerStock(Stock leStock)
        {
            return _lstStock.Remove(leStock);
        }

        /// <summary>
        /// Méthode permettant de savoir si l'aliment envoyé en paramètre est disponible en inventaire
        /// </summary>
        /// <param name="pAliment"></param>
        /// <returns>Vrai si il est disponible sinon Faux</returns>
        public bool AlimentEstDisponible(Aliment pAliment)
        {
            foreach (Stock stock in LstStock)
            {
                if (stock.Aliment.Nom == pAliment.Nom && stock.Status == Status.Disponible)
                {
                   return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Méthode permettant d'obtenir une liste des stocks dans aliments disponible en inventaire
        /// </summary>
        /// <returns>Liste de stock disponible en inventaire</returns>
        public List<Stock> ObtenirListeAlimentDisponible()
        {
            List<Stock> listeAlimentEtQtty = new List<Stock>();
            foreach (Stock stock in _lstStock)
            {
                if (stock.Status == Status.Disponible)
                {
                    listeAlimentEtQtty.Add(stock);
                }
            }

            return listeAlimentEtQtty;
        }

        /// <summary>
        /// Méthode permettant d'obtenir un Aliment de l'inventaire selon le nom de la recette en paramètre
        /// </summary>
        /// <param name="nomAliment">chaîne de caractère représentant le nom de l'aliment</param>
        /// <returns>Objet Aliment ayant comme propriété Nom la chaîne de caractère envoyé en paramètre</returns>
        public Aliment ObtenirAlimentSelonNom(string nomAliment)
        {
            foreach (Stock stock in this._lstStock)
            {
                if (stock.Aliment.Nom.ToLower() == nomAliment.ToLower())
                {
                    return stock.Aliment;
                }
            }

            throw new ArgumentException("L'aliment envoyé en paramètre n'existe pas");
        }

        /// <summary>
        /// Permet d'obtenir une liste de stock de chaque aliment en inventaire correspondant aux filtres reçus en paramètres
        /// </summary>
        /// <param name="pFiltresChoisis">Liste de Dictionnaire composé des filtres et de leur valeur</param>
        /// <returns>Liste de stock de chaque aliment répondant aux critères du filtre de l'utilisateur</returns>
        public List<Stock> ObtenirListeSelonFiltres(ListDictionary pFiltresChoisis)
        {
            List<Stock> listeResultats = new List<Stock>();

            if (pFiltresChoisis.Contains("Barre recherche"))
            {
                foreach (Stock stock in LstStock)
                {
                    if (stock.Aliment.Nom.ToLower().Contains(pFiltresChoisis["Barre recherche"].ToString().ToLower()))
                    {
                        listeResultats.Add(stock);
                    }
                }
            }
            else
            {
                foreach (Stock stock in this.LstStock)
                {
                    listeResultats.Add(stock);
                }
            }

            if (pFiltresChoisis.Contains("Statut"))
            {
                for (int i = 0; i < listeResultats.Count; i++)
                {
                    if (listeResultats[i].Status != (Status)pFiltresChoisis["Statut"])
                    {
                        listeResultats.RemoveAt(i);
                        i--;
                    }
                }
            }

            if (pFiltresChoisis.Contains("Catégorie"))
            {
                for (int i = 0; i < listeResultats.Count; i++)
                {
                    if (listeResultats[i].Aliment.CategorieAliment != (CategorieAliment)pFiltresChoisis["Catégorie"])
                    {
                        listeResultats.RemoveAt(i);
                        i--;
                    }
                }
            }

            return listeResultats;
        }

        #endregion Méthodes
    }
}