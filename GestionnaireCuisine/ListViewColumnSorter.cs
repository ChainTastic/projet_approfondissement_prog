﻿using System.Collections;
using System.Windows.Forms;

public class ListViewColumnSorter : IComparer
{
    #region Attributs

    private int _ColonneATrier;
    private SortOrder _OrdreDeTrie;
    private CaseInsensitiveComparer _ObjectCompare;

    #endregion Attributs

    #region Accesseurs

    public int SortColumn
    {
        get { return this._ColonneATrier; }
        set { this._ColonneATrier = value; }
    }

    public SortOrder Order
    {
        get { return this._OrdreDeTrie; }
        set { this._OrdreDeTrie = value; }
    }

    #endregion Accesseurs

    #region Constructeurs

    public ListViewColumnSorter()
    {
        this._ColonneATrier = 0;
        this._OrdreDeTrie = SortOrder.None;
        this._ObjectCompare = new CaseInsensitiveComparer();
    }

    #endregion Constructeurs

    public int Compare(object x, object y)
    {
        if (!(x is ListViewItem))
        {
            return (0);
        }

        if (!(y is ListViewItem))
        {
            return (0);
        }

        ListViewItem item1 = (ListViewItem)x;
        ListViewItem item2 = (ListViewItem)y;

        if (item1.ListView.Columns[this._ColonneATrier].Tag == null)
        {
            item1.ListView.Columns[this._ColonneATrier].Tag = "Text";
        }

        if (item1.ListView.Columns[this._ColonneATrier].Tag.ToString() == "Numeric")
        {
            float fl1 = float.Parse(item1.SubItems[this.SortColumn].Text.Split(" ")[0]);
            float fl2 = float.Parse(item2.SubItems[this.SortColumn].Text.Split(" ")[0]);

            if (this.Order == SortOrder.Ascending)
            {
                return fl1.CompareTo(fl2);
            }
            else
            {
                return -fl1.CompareTo(fl2);
            }
        }
        else
        {
            string str1 = item1.SubItems[this.SortColumn].Text;
            string str2 = item2.SubItems[this.SortColumn].Text;

            if (this.Order == SortOrder.Ascending)
            {
                return str1.CompareTo(str2);
            }
            else
            {
                return str2.CompareTo(str1);
            }
        }
    }
}