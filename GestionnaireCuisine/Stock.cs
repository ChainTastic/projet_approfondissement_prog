﻿using System;

namespace GestionnaireCuisine
{
    public enum Status
    {
        Disponible,
        NonDisponible,
        Perime
    }

    public enum UniteMesure
    {
        Millilitre,
        Litre,
        Gramme,
        Kilogramme,
        Once,
        Livre,
        Unité
    }

    public class Stock
    {
        #region Attributs

        private Aliment _aliment;
        private float _quantite;
        private Status _status;
        private UniteMesure _uniteMesure;

        #endregion Attributs

        #region Accesseurs

        public Aliment Aliment
        {
            get { return _aliment; }
            set { _aliment = value; }
        }

        public float Quantite
        {
            get { return _quantite; }
            set { _quantite = value; }
        }

        public UniteMesure UniteMesure
        {
            get { return _uniteMesure; }
            set { _uniteMesure = value; }
        }

        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string QuantiteFormatte
        {
            get
            {
                return String.Format("{0} {1}", this.Quantite,
                    FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(UniteMesure));
            }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Stock(Aliment pAliment, float pQuantite, UniteMesure pUniteMesure, Status pStatus = Status.Disponible)
        {
            Aliment = pAliment;
            Quantite = pQuantite;
            Status = pStatus;
            UniteMesure = pUniteMesure;
        }

        public Stock()
        {
            Aliment = _aliment;
            Status = _status;
        }

        #endregion Constructeurs

        #region Méthodes
        #endregion
    }
}