﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using GestionnaireCuisine.Properties;

namespace GestionnaireCuisine
{
    public partial class FenetreRecette : Form
    {
        #region Attributs

        private Recette _laRecette;
        private Inventaire _lInventaire;
        private string _imageLocation;
        private List<(Aliment, float, UniteMesure)> _lstAlimentQtty;

        #endregion Attributs

        #region Accesseurs

        public Recette LaRecette
        {
            get { return _laRecette; }
            set
            {
                _laRecette = value;
                txtNomRecette.Text = _laRecette.Nom;
                foreach ((Aliment, float, UniteMesure) aliment in _laRecette.LstAlimentsQtty)
                {
                    string[] arrInfoAliment = new string[2];
                    arrInfoAliment[0] = aliment.Item1.Nom;
                    arrInfoAliment[1] = String.Format("{0} {1}",aliment.Item2, FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(aliment.Item3));
                    ListViewItem item = new ListViewItem(arrInfoAliment);
                    lstVwIngredientsRecette.Items.Add(item);
                    _lstAlimentQtty.Add((aliment.Item1, aliment.Item2, aliment.Item3));
                }

                _imageLocation = _laRecette.NomImage;
                pbRecette.Image = Image.FromFile(_imageLocation);
            }
        }

        #endregion Accesseurs

        #region Constructeurs

        public FenetreRecette()
        {
            InitializeComponent();
            _lInventaire = FenetrePrincipal.LeGestionnaire.Inventaire;
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = btnOK;
            CancelButton = btnAnnuler;
            btnOK.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
            _lstAlimentQtty = new List<(Aliment, float, UniteMesure)>();
        }

        #endregion Constructeurs

        #region Événements

        private void btnImporterImage_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "fichier JPEG(*.jpg)|*.jpg|fichier PNG(*.png)|*.png| Tout les fichiers(*.*)|*.*";

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    _imageLocation = dialog.FileName;
                    pbRecette.ImageLocation = _imageLocation;
                }

                dialog.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur c'est produite!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FenetreRecette_Load(object sender, EventArgs e)
        {
            lstVwIngredientsRecette.GridLines = true;
            lstVwIngredientsRecette.FullRowSelect = true;
            lstVwIngredientDispo.GridLines = true;
            lstVwIngredientDispo.FullRowSelect = true;
            if (_laRecette == null)
            {
                pbRecette.Image = Resources.livreDeCuisine;
            }
            else
            {
                pbRecette.Image = Image.FromFile(_imageLocation);
            }

            foreach (Stock stock in _lInventaire.LstStock)
            {
                string[] arrInfoAliment = new string[2];
                arrInfoAliment[0] = stock.Aliment.Nom;
                arrInfoAliment[1] = stock.QuantiteFormatte;
                ListViewItem item = new ListViewItem(arrInfoAliment);
                lstVwIngredientDispo.Items.Add(item);
            }
        }

        //TODO factorisé le code
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (_laRecette == null)
                {
                    _laRecette = new Recette(txtNomRecette.Text);
                    foreach ((Aliment, float, UniteMesure) valueTuple in _lstAlimentQtty)
                    {
                        _laRecette.AjouterAliment(valueTuple);
                    }

                    if (_imageLocation != null)
                    {
                        try
                        {
                            GC.Collect();

                            GC.WaitForPendingFinalizers();
                            File.Copy(_imageLocation,
                                Path.Combine("..//../..//Images//", Path.GetFileName(_imageLocation)), true);

                            _laRecette.NomImage =
                                Path.Combine("..//../..//Images//", Path.GetFileName(_imageLocation));
                        }
                        catch (IOException exception)
                        {
                            MessageBox.Show(exception.Message);
                        }
                    }
                    else
                    {
                        string basePath = AppDomain.CurrentDomain.BaseDirectory;
                        string path = string.Format("{0}Resources\\livreDeCuisine.png",
                            Path.GetFullPath(Path.Combine(basePath, @"..\..\..\")));
                        string relativePath = Path.GetRelativePath(basePath, path);
                        this._laRecette.NomImage = relativePath;
                    }
                }
                else
                {
                    _laRecette.Nom = txtNomRecette.Text;
                    _laRecette.LstAlimentsQtty = new List<(Aliment, float, UniteMesure)>(_lstAlimentQtty);
                    if (_imageLocation != null)
                    {
                        if (_imageLocation != _laRecette.NomImage)
                        {
                            try
                            {
                                GC.Collect();

                                GC.WaitForPendingFinalizers();
                                File.Copy(_imageLocation,
                                    Path.Combine("..//../..//Images//", Path.GetFileName(_imageLocation)), true);
                                _laRecette.NomImage = Path.Combine("..//../..//Images//",
                                    Path.GetFileName(_imageLocation));
                            }
                            catch (IOException exception)
                            {
                                MessageBox.Show(exception.Message);
                            }
                        }
                    }
                    else
                    {
                        string basePath = AppDomain.CurrentDomain.BaseDirectory;
                        string path = string.Format("{0}Resources\\livreDeCuisine.png",
                            Path.GetFullPath(Path.Combine(basePath, @"..\..\..\")));
                        string relativePath = Path.GetRelativePath(basePath, path);
                        this._laRecette.NomImage = relativePath;
                    }
                }
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message);

                if (argumentException.Message.Contains("nom"))
                {
                    txtNomRecette.Focus();
                }

                this.DialogResult = DialogResult.None;
            }
        }

        private void btnAjouterIngredient_Click(object sender, EventArgs e)
        {
            if (lstVwIngredientDispo.SelectedItems.Count > 0)
            {
                if (cbBoxUniteMesure.SelectedItem != null)
                {
                    ListViewItem ingredientSelectionne = lstVwIngredientDispo.SelectedItems[0];
                    Aliment aliment = _lInventaire.ObtenirAlimentSelonNom(ingredientSelectionne.SubItems[0].Text);
                    Tuple<Aliment, float, UniteMesure> tupleAlimentQtty = new Tuple<Aliment, float, UniteMesure>(aliment,
                        (float)Convert.ToDouble(numQttyIngredientNecessaire.Text), (UniteMesure) cbBoxUniteMesure.SelectedItem);
                    if (_laRecette != null)
                    {
                        if (_laRecette.ContientAliment(tupleAlimentQtty.Item1.Nom))
                        {
                            MessageBox.Show("Votre recette contient déjà cet ingrédient", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    _lstAlimentQtty.Add((tupleAlimentQtty.Item1, tupleAlimentQtty.Item2, tupleAlimentQtty.Item3));
                    string[] arrInfoAliment = new string[2];
                    arrInfoAliment[0] = tupleAlimentQtty.Item1.Nom;
                    arrInfoAliment[1] = String.Format("{0} {1}", tupleAlimentQtty.Item2,
                        FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(tupleAlimentQtty.Item3));
                    ListViewItem item = new ListViewItem(arrInfoAliment);
                    lstVwIngredientsRecette.Items.Add(item);


                }
                else
                {
                    MessageBox.Show("Vous devez sélectionner une unité de mesure pour pouvoir l'ajouter à la recette.",
                        "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner un aliment disponible pour pouvoir l'ajouter à la recette.",
                    "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void btnRetirerIngredientRecette_Click(object sender, EventArgs e)
        {
            if (lstVwIngredientsRecette.SelectedItems.Count > 0)
            {
                ListViewItem ingredientSelectionne = lstVwIngredientsRecette.SelectedItems[0];
                Aliment aliment = _lInventaire.ObtenirAlimentSelonNom(ingredientSelectionne.SubItems[0].Text);
                (Aliment, float, UniteMesure) tupleAlimentQtty = (aliment,
                    Convert.ToSingle(ingredientSelectionne.SubItems[1].Text.Split(" ")[0]),
                    _lInventaire.GetStockSelonAliment(aliment).UniteMesure);
                for (int i = 0; i < _lstAlimentQtty.Count; i++)
                {
                    if (_lstAlimentQtty[i].Item1.Nom == tupleAlimentQtty.Item1.Nom &&
                        _lstAlimentQtty[i].Item2 == tupleAlimentQtty.Item2)
                    {
                        _lstAlimentQtty.Remove(tupleAlimentQtty);
                        lstVwIngredientsRecette.Items.Remove(ingredientSelectionne);
                    }
                }
            }
            else
            {
                MessageBox.Show("Vous devez sélectionner un aliment de la recette pour pouvoir la retirer.", "Erreur",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion Événements

        private void numQttyIngredientNecessaire_ValueChanged(object sender, EventArgs e)
        {
            if (lstVwIngredientsRecette.SelectedItems.Count > 0)
            {
                int indiceAliment = lstVwIngredientsRecette.Items.IndexOf(lstVwIngredientsRecette.SelectedItems[0]);

                if (numQttyIngredientNecessaire.Value != (decimal) _lstAlimentQtty[indiceAliment].Item2)
                {
                    string sigle = lstVwIngredientsRecette.SelectedItems[0].SubItems[1].Text.Split(" ")[1];
                    UniteMesure uniteIngredientSelectionne = FenetrePrincipal.LeGestionnaire.ObtenirUniteSelonSigle(sigle);
                    ListViewItem item = lstVwIngredientsRecette.Items[indiceAliment];
                    Aliment aliment = _lInventaire.ObtenirAlimentSelonNom(item.SubItems[0].Text);
                    (Aliment, float, UniteMesure) tupleAlimentQtty = (aliment, Convert.ToSingle(item.SubItems[1].Text.Split(" ")[0]),
                        uniteIngredientSelectionne);
                    tupleAlimentQtty.Item2 = (float)numQttyIngredientNecessaire.Value;
                    _lstAlimentQtty[indiceAliment] = tupleAlimentQtty;
                    lstVwIngredientsRecette.Items[indiceAliment].SubItems[1].Text = String.Format("{0} {1}",
                        tupleAlimentQtty.Item2.ToString(CultureInfo.CurrentCulture),
                        FenetrePrincipal.LeGestionnaire.ObtenirUniteMesureFormatte(tupleAlimentQtty.Item3));
                }
            }
        }

        private void lstVwIngredientsRecette_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstVwIngredientsRecette.SelectedItems.Count > 0)
            {
                numQttyIngredientNecessaire.Text = lstVwIngredientsRecette.SelectedItems[0].SubItems[1].Text.Split(" ")[0];
            }
        }

        private void lstVwIngredientDispo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstVwIngredientDispo.SelectedItems.Count > 0)
            {
                cbBoxUniteMesure.Items.Clear();
                string sigle = lstVwIngredientDispo.SelectedItems[0].SubItems[1].Text.Split(" ")[1];
                UniteMesure uniteIngredientSelectionne = FenetrePrincipal.LeGestionnaire.ObtenirUniteSelonSigle(sigle);
                if (uniteIngredientSelectionne == UniteMesure.Gramme || uniteIngredientSelectionne == UniteMesure.Kilogramme)
                {
                    cbBoxUniteMesure.Items.Add(UniteMesure.Gramme);
                    cbBoxUniteMesure.Items.Add(UniteMesure.Kilogramme);
                }
                else if (uniteIngredientSelectionne == UniteMesure.Millilitre ||
                         uniteIngredientSelectionne == UniteMesure.Litre)
                {
                    cbBoxUniteMesure.Items.Add(UniteMesure.Millilitre);
                    cbBoxUniteMesure.Items.Add(UniteMesure.Litre);
                }
                else
                {
                    cbBoxUniteMesure.Items.Add(uniteIngredientSelectionne);
                }
            }
        }
    }
}