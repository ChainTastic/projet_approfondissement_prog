﻿using System;

namespace GestionnaireCuisine
{
    public enum CategorieAliment
    {
        ViandeEtSubstitut = 3,
        FruitEtLegume = 5,
        ProduitLaitier = 6,
        ProduitCerealiers = 7,
        Oeufs = 30,
        DiversSecs = 9
    }

    public class Aliment
    {
        #region Attributs

        private string _nom;
        private CategorieAliment _categorieAliment;
        private DateTime _dateReception;

        #endregion Attributs

        #region Accesseurs

        public string Nom
        {
            get { return this._nom; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Le nom est vide");
                }

                this._nom = value;

                string[] nomAlimentDecomposee = value.Split(" ");

                foreach (string mot in nomAlimentDecomposee)
                {
                    foreach (char c in mot)
                    {
                        if (!char.IsLetter(c))
                        {
                            throw new ArgumentException("Le nom de l'aliment doit être composé que de lettre");
                        }
                    }
                }
            }
        }

        public CategorieAliment CategorieAliment
        {
            get { return this._categorieAliment; }
            set { this._categorieAliment = value; }
        }

        public DateTime DateReception
        {
            get { return this._dateReception; }
            set { this._dateReception = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Aliment(string pNom, CategorieAliment pCategorieAliment)
        {
            Nom = pNom;
            CategorieAliment = pCategorieAliment;
            DateReception = DateTime.Today;
        }

        public Aliment()
        {
        }

        public Aliment(string pNom)
        {
            this.Nom = pNom;
            this.DateReception = DateTime.Today;
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Permet d'obtenir la date de péremption de l'aliment
        /// </summary>
        /// <returns>La date de péremption de l'aliment</returns>
        private DateTime GetDatePeremption()
        {
            return this.DateReception.AddDays((double)this.CategorieAliment);
        }

        /// <summary>
        /// Permet d'obtenir le temps de conservation restant en jours
        /// </summary>
        /// <returns>Nombre de jours restant avant la date de péremption</returns>
        public int ObtenirTempsConservationRestant()
        {
            DateTime aujourdhui = DateTime.Today;

            return (Math.Max(0, (GetDatePeremption() - aujourdhui).Days));
        }

        #endregion Méthodes
    }
}