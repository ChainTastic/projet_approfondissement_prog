﻿namespace GestionnaireCuisine
{
    partial class FenetreListeProduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnProduireListeProd = new System.Windows.Forms.Button();
            this.btnExporterPDF = new System.Windows.Forms.Button();
            this.btnReinitialiserListeProd = new System.Windows.Forms.Button();
            this.lblListeRecetteAProduire = new System.Windows.Forms.Label();
            this.lblListeProduction = new System.Windows.Forms.Label();
            this.colChoixRecette = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colQttyRecette = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grilleRecette = new System.Windows.Forms.DataGridView();
            this.col_aliment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQtty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColQttyDispo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColQttyCommander = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grilleProduction = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.grilleRecette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grilleProduction)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(952, 403);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(78, 31);
            this.btnAnnuler.TabIndex = 2;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(1036, 403);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(78, 31);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnProduireListeProd
            // 
            this.btnProduireListeProd.Location = new System.Drawing.Point(955, 150);
            this.btnProduireListeProd.Name = "btnProduireListeProd";
            this.btnProduireListeProd.Size = new System.Drawing.Size(159, 39);
            this.btnProduireListeProd.TabIndex = 4;
            this.btnProduireListeProd.Text = "Simuler production des aliments ";
            this.btnProduireListeProd.UseVisualStyleBackColor = true;
            this.btnProduireListeProd.Click += new System.EventHandler(this.btnProduireListeProd_Click);
            // 
            // btnExporterPDF
            // 
            this.btnExporterPDF.Location = new System.Drawing.Point(955, 285);
            this.btnExporterPDF.Name = "btnExporterPDF";
            this.btnExporterPDF.Size = new System.Drawing.Size(159, 39);
            this.btnExporterPDF.TabIndex = 5;
            this.btnExporterPDF.Text = "Exporter en PDF";
            this.btnExporterPDF.UseVisualStyleBackColor = true;
            this.btnExporterPDF.Click += new System.EventHandler(this.btnExporterPDF_Click);
            // 
            // btnReinitialiserListeProd
            // 
            this.btnReinitialiserListeProd.Location = new System.Drawing.Point(955, 225);
            this.btnReinitialiserListeProd.Name = "btnReinitialiserListeProd";
            this.btnReinitialiserListeProd.Size = new System.Drawing.Size(159, 39);
            this.btnReinitialiserListeProd.TabIndex = 6;
            this.btnReinitialiserListeProd.Text = "Réinitialiser liste de production";
            this.btnReinitialiserListeProd.UseVisualStyleBackColor = true;
            this.btnReinitialiserListeProd.Click += new System.EventHandler(this.btnReinitialiserListeProd_Click);
            // 
            // lblListeRecetteAProduire
            // 
            this.lblListeRecetteAProduire.AutoSize = true;
            this.lblListeRecetteAProduire.Location = new System.Drawing.Point(13, 28);
            this.lblListeRecetteAProduire.Name = "lblListeRecetteAProduire";
            this.lblListeRecetteAProduire.Size = new System.Drawing.Size(159, 15);
            this.lblListeRecetteAProduire.TabIndex = 7;
            this.lblListeRecetteAProduire.Text = "Liste des recettes à produire :";
            // 
            // lblListeProduction
            // 
            this.lblListeProduction.AutoSize = true;
            this.lblListeProduction.Location = new System.Drawing.Point(572, 28);
            this.lblListeProduction.Name = "lblListeProduction";
            this.lblListeProduction.Size = new System.Drawing.Size(109, 15);
            this.lblListeProduction.TabIndex = 8;
            this.lblListeProduction.Text = "Liste de production";
            // 
            // colChoixRecette
            // 
            this.colChoixRecette.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colChoixRecette.HeaderText = "Recette";
            this.colChoixRecette.Name = "colChoixRecette";
            this.colChoixRecette.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colQttyRecette
            // 
            this.colQttyRecette.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colQttyRecette.HeaderText = "Quantité de recette prévue";
            this.colQttyRecette.Name = "colQttyRecette";
            this.colQttyRecette.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colQttyRecette.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // grilleRecette
            // 
            this.grilleRecette.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grilleRecette.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChoixRecette,
            this.colQttyRecette});
            this.grilleRecette.Location = new System.Drawing.Point(12, 57);
            this.grilleRecette.Name = "grilleRecette";
            this.grilleRecette.Size = new System.Drawing.Size(286, 377);
            this.grilleRecette.TabIndex = 1;
            this.grilleRecette.Text = "dataGridView2";
            // 
            // col_aliment
            // 
            this.col_aliment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.col_aliment.HeaderText = "Aliment";
            this.col_aliment.Name = "col_aliment";
            this.col_aliment.ReadOnly = true;
            this.col_aliment.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // colQtty
            // 
            this.colQtty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colQtty.HeaderText = "Quantité possible à produire";
            this.colQtty.Name = "colQtty";
            this.colQtty.ReadOnly = true;
            this.colQtty.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColQttyDispo
            // 
            this.ColQttyDispo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColQttyDispo.HeaderText = "Quantité disponible après production";
            this.ColQttyDispo.Name = "ColQttyDispo";
            this.ColQttyDispo.ReadOnly = true;
            this.ColQttyDispo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ColQttyCommander
            // 
            this.ColQttyCommander.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColQttyCommander.HeaderText = "Quantité à commander";
            this.ColQttyCommander.Name = "ColQttyCommander";
            this.ColQttyCommander.ReadOnly = true;
            this.ColQttyCommander.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // FenetreListeProduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 446);
            // 
            // grilleProduction
            // 
            this.grilleProduction.AllowUserToAddRows = false;
            this.grilleProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grilleProduction.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_aliment,
            this.colQtty,
            this.ColQttyDispo,
            this.ColQttyCommander});
            this.grilleProduction.Location = new System.Drawing.Point(333, 57);
            this.grilleProduction.Name = "grilleProduction";
            this.grilleProduction.ReadOnly = true;
            this.grilleProduction.Size = new System.Drawing.Size(613, 377);
            this.grilleProduction.TabIndex = 0;
            this.grilleProduction.Text = "dataGridView1";
            this.Controls.Add(this.lblListeProduction);
            this.Controls.Add(this.lblListeRecetteAProduire);
            this.Controls.Add(this.btnReinitialiserListeProd);
            this.Controls.Add(this.btnExporterPDF);
            this.Controls.Add(this.btnProduireListeProd);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.grilleRecette);
            this.Controls.Add(this.grilleProduction);
            this.Name = "FenetreListeProduction";
            this.Text = "Liste de production";
            this.Load += new System.EventHandler(this.FenetreListeProduction_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grilleRecette)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grilleProduction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView grilleProduction;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnProduireListeProd;
        private System.Windows.Forms.DataGridView grilleRecette;
        private System.Windows.Forms.Button btnExporterPDF;
        private System.Windows.Forms.Button btnReinitialiserListeProd;
        private System.Windows.Forms.Label lblListeRecetteAProduire;
        private System.Windows.Forms.Label lblListeProduction;
        private System.Windows.Forms.DataGridViewComboBoxColumn colChoixRecette;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQttyRecette;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_aliment;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQtty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColQttyDispo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColQttyCommander;
    }
}