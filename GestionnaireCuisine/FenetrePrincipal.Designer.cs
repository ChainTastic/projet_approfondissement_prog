﻿namespace GestionnaireCuisine
{
    partial class FenetrePrincipal
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRecette = new System.Windows.Forms.Button();
            this.btnInventaire = new System.Windows.Forms.Button();
            this.btnListeProduction = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.lblTitre = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRecette
            // 
            this.btnRecette.Font = new System.Drawing.Font("MS Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnRecette.Location = new System.Drawing.Point(0, 288);
            this.btnRecette.Name = "btnRecette";
            this.btnRecette.Size = new System.Drawing.Size(672, 75);
            this.btnRecette.TabIndex = 0;
            this.btnRecette.Text = "Livre de recette";
            this.btnRecette.UseVisualStyleBackColor = true;
            this.btnRecette.Click += new System.EventHandler(this.btnRecette_Click);
            // 
            // btnInventaire
            // 
            this.btnInventaire.Font = new System.Drawing.Font("MS Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnInventaire.Location = new System.Drawing.Point(0, 166);
            this.btnInventaire.Name = "btnInventaire";
            this.btnInventaire.Size = new System.Drawing.Size(672, 75);
            this.btnInventaire.TabIndex = 0;
            this.btnInventaire.Text = "Inventaire";
            this.btnInventaire.UseVisualStyleBackColor = true;
            this.btnInventaire.Click += new System.EventHandler(this.btnInventaire_Click);
            // 
            // btnListeProduction
            // 
            this.btnListeProduction.Font = new System.Drawing.Font("MS Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnListeProduction.Location = new System.Drawing.Point(0, 414);
            this.btnListeProduction.Name = "btnListeProduction";
            this.btnListeProduction.Size = new System.Drawing.Size(672, 75);
            this.btnListeProduction.TabIndex = 0;
            this.btnListeProduction.Text = "Liste de production";
            this.btnListeProduction.UseVisualStyleBackColor = true;
            this.btnListeProduction.Click += new System.EventHandler(this.btnListeProduction_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.BackColor = System.Drawing.Color.Red;
            this.btnQuitter.Font = new System.Drawing.Font("MS Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnQuitter.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnQuitter.Location = new System.Drawing.Point(0, 643);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(672, 55);
            this.btnQuitter.TabIndex = 0;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = false;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // lblTitre
            // 
            this.lblTitre.BackColor = System.Drawing.SystemColors.Highlight;
            this.lblTitre.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitre.Font = new System.Drawing.Font("MS Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTitre.Location = new System.Drawing.Point(0, 0);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(672, 126);
            this.lblTitre.TabIndex = 1;
            this.lblTitre.Text = "Gestionnaire de cuisine";
            this.lblTitre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(0, 535);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(672, 75);
            this.button1.TabIndex = 2;
            this.button1.Text = "Tutoriel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FenetrePrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 710);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnListeProduction);
            this.Controls.Add(this.btnInventaire);
            this.Controls.Add(this.btnRecette);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FenetrePrincipal";
            this.Text = "Gestionnaire de cuisine";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRecette;
        private System.Windows.Forms.Button btnInventaire;
        private System.Windows.Forms.Button btnListeProduction;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.Button button1;
    }
}

