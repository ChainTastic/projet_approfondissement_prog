﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using LibVLCSharp.Shared;

namespace GestionnaireCuisine
{
    public partial class FenetreTutoriel : Form
    {
        private LibVLC _libVlc;
        private MediaPlayer _mp;

        private bool _isFullscreen;
        private bool _isPlaying;
        private Size oldVideoSize;
        private Size oldFormSize;
        private Point oldVideoLocation;

        public FenetreTutoriel()
        {
            InitializeComponent();
            Core.Initialize();
            KeyPreview = true;
            KeyDown += ShortcutEvent;
            oldVideoSize = videoView1.Size;
            oldFormSize = Size;
            oldVideoLocation = videoView1.Location;
            _libVlc = new LibVLC();
            _mp = new MediaPlayer(_libVlc);
            videoView1.MediaPlayer = _mp;
            AcceptButton = btnMenuPrincipale;
            btnMenuPrincipale.DialogResult = DialogResult.OK;
        }

        #region Événements

        private void Tutoriel_Load(object sender, EventArgs e)
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            string path = string.Format("{0}Resources\\Tutoriel.mp4",
                Path.GetFullPath(Path.Combine(basePath, @"..\..\..\")));
            string relativePath = Path.GetRelativePath(basePath, path);
            JouerFichier(relativePath);
        }

        private void FenetreTutoriel_FormClosing(object sender, FormClosingEventArgs e)
        {
            _mp.Stop();
        }

        private void btnPausePlay_Click(object sender, EventArgs e)
        {
            if (_mp.State == VLCState.Playing)
            {
                _mp.Pause();
                btnPausePlay.Text = "Jouer";
            }
            else
            {
                _mp.Play();
                btnPausePlay.Text = "Pause";
            }
        }

        private void fullscreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GoFullScreen();
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuPrincipaletoolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        #region Méthodes

        /// <summary>
        /// Permet de gérer les racourcis claviers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShortcutEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11 && !_isFullscreen)
            {
                GoFullScreen();
            }
            else if ((e.KeyCode == Keys.Escape || e.KeyCode == Keys.F11) && _isFullscreen)
            {
                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
                Size = oldFormSize;
                menuStrip1.Visible = true;
                btnMenuPrincipale.Visible = true;
                btnPausePlay.Visible = true;
                videoView1.Size = oldVideoSize;
                videoView1.Location = oldVideoLocation;
                _isFullscreen = false;
            }

            if (_isPlaying)
            {
                if (e.KeyCode == Keys.Space)
                {
                    if (_mp.State == VLCState.Playing)
                    {
                        _mp.Pause();
                    }
                    else
                    {
                        _mp.Play();
                    }
                }

                if (e.KeyCode == Keys.J)
                {
                    _mp.Position -= 0.01f;
                }

                if (e.KeyCode == Keys.L)
                {
                    _mp.Position += 0.01f;
                }
            }
        }

        /// <summary>
        /// Joue le fichier vidéo à partir du chemin d'accès reçue en commentaire
        /// </summary>
        /// <param name="file">chemin d'accès du fichier vidéo</param>
        private void JouerFichier(string file)
        {
            _mp.Play(new Media(_libVlc, file));
            _isPlaying = true;
        }

        /// <summary>
        /// Permet de modifier le formualire pour une lecture en mode plein écran
        /// </summary>
        private void GoFullScreen()
        {
            btnMenuPrincipale.Visible = false;
            btnPausePlay.Visible = false;
            menuStrip1.Visible = false;
            videoView1.Size = Size;
            videoView1.Location = new Point(0, 0);
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            _isFullscreen = true;
        }

        #endregion
    }
}