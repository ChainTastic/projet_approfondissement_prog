﻿namespace GestionnaireCuisine
{
    partial class FenetreInventaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAjouterAliment = new System.Windows.Forms.Button();
            this.lstInventaire = new System.Windows.Forms.ListView();
            this.colHeaderNom = new System.Windows.Forms.ColumnHeader();
            this.colHeaderStatus = new System.Windows.Forms.ColumnHeader();
            this.colHeaderQtty = new System.Windows.Forms.ColumnHeader();
            this.colHeaderPeremption = new System.Windows.Forms.ColumnHeader();
            this.btnSupprimerAliment = new System.Windows.Forms.Button();
            this.numQttyAliment = new System.Windows.Forms.NumericUpDown();
            this.lblQttyAliment = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.gboFiltres = new System.Windows.Forms.GroupBox();
            this.btnReinitialiserFiltres = new System.Windows.Forms.Button();
            this.btnFiltrer = new System.Windows.Forms.Button();
            this.gboFiltreSelonStatut = new System.Windows.Forms.GroupBox();
            this.chkBoxFiltreStatusAliment = new System.Windows.Forms.CheckBox();
            this.gboFiltreTypeAliment = new System.Windows.Forms.GroupBox();
            this.cboTypesAliment = new System.Windows.Forms.ComboBox();
            this.chkBoxFiltreTypeAliment = new System.Windows.Forms.CheckBox();
            this.gboFiltreBarreRecherche = new System.Windows.Forms.GroupBox();
            this.txtRecherche = new System.Windows.Forms.TextBox();
            this.chkBoxFiltreBarreRecherche = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numQttyAliment)).BeginInit();
            this.gboFiltres.SuspendLayout();
            this.gboFiltreSelonStatut.SuspendLayout();
            this.gboFiltreTypeAliment.SuspendLayout();
            this.gboFiltreBarreRecherche.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAjouterAliment
            // 
            this.btnAjouterAliment.Location = new System.Drawing.Point(793, 117);
            this.btnAjouterAliment.Name = "btnAjouterAliment";
            this.btnAjouterAliment.Size = new System.Drawing.Size(137, 40);
            this.btnAjouterAliment.TabIndex = 0;
            this.btnAjouterAliment.Text = "Ajouter Aliment";
            this.btnAjouterAliment.UseVisualStyleBackColor = true;
            this.btnAjouterAliment.Click += new System.EventHandler(this.btnAjouterAliment_Click);
            // 
            // lstInventaire
            // 
            this.lstInventaire.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeaderNom,
            this.colHeaderStatus,
            this.colHeaderQtty,
            this.colHeaderPeremption});
            this.lstInventaire.ForeColor = System.Drawing.Color.White;
            this.lstInventaire.HideSelection = false;
            this.lstInventaire.Location = new System.Drawing.Point(205, 59);
            this.lstInventaire.MultiSelect = false;
            this.lstInventaire.Name = "lstInventaire";
            this.lstInventaire.Size = new System.Drawing.Size(554, 442);
            this.lstInventaire.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstInventaire.TabIndex = 1;
            this.lstInventaire.UseCompatibleStateImageBehavior = false;
            this.lstInventaire.View = System.Windows.Forms.View.Details;
            this.lstInventaire.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstInventaire_ColumnClick);
            this.lstInventaire.SelectedIndexChanged += new System.EventHandler(this.lstInventaire_SelectedIndexChanged);
            // 
            // colHeaderNom
            // 
            this.colHeaderNom.Text = "Nom";
            this.colHeaderNom.Width = 190;
            // 
            // colHeaderStatus
            // 
            this.colHeaderStatus.Text = "Status";
            this.colHeaderStatus.Width = 90;
            // 
            // colHeaderQtty
            // 
            this.colHeaderQtty.Tag = "Numeric";
            this.colHeaderQtty.Text = "Quantité";
            this.colHeaderQtty.Width = 90;
            // 
            // colHeaderPeremption
            // 
            this.colHeaderPeremption.Tag = "Numeric";
            this.colHeaderPeremption.Text = "Jours restants avant péremption";
            this.colHeaderPeremption.Width = 180;
            // 
            // btnSupprimerAliment
            // 
            this.btnSupprimerAliment.Location = new System.Drawing.Point(793, 200);
            this.btnSupprimerAliment.Name = "btnSupprimerAliment";
            this.btnSupprimerAliment.Size = new System.Drawing.Size(137, 40);
            this.btnSupprimerAliment.TabIndex = 2;
            this.btnSupprimerAliment.Text = "Supprimer Aliment";
            this.btnSupprimerAliment.UseVisualStyleBackColor = true;
            this.btnSupprimerAliment.Click += new System.EventHandler(this.btnSupprimerAliment_Click);
            // 
            // numQttyAliment
            // 
            this.numQttyAliment.DecimalPlaces = 2;
            this.numQttyAliment.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numQttyAliment.Location = new System.Drawing.Point(867, 331);
            this.numQttyAliment.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numQttyAliment.Name = "numQttyAliment";
            this.numQttyAliment.Size = new System.Drawing.Size(72, 23);
            this.numQttyAliment.TabIndex = 9;
            this.numQttyAliment.Leave += new System.EventHandler(this.numQttyAliment_Leave);
            // 
            // lblQttyAliment
            // 
            this.lblQttyAliment.AutoSize = true;
            this.lblQttyAliment.Location = new System.Drawing.Point(793, 333);
            this.lblQttyAliment.Name = "lblQttyAliment";
            this.lblQttyAliment.Size = new System.Drawing.Size(65, 15);
            this.lblQttyAliment.TabIndex = 8;
            this.lblQttyAliment.Text = "Quantité :  ";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(861, 468);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 33);
            this.btnOK.TabIndex = 10;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(780, 468);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 33);
            this.btnAnnuler.TabIndex = 11;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            // 
            // gboFiltres
            // 
            this.gboFiltres.Controls.Add(this.btnReinitialiserFiltres);
            this.gboFiltres.Controls.Add(this.btnFiltrer);
            this.gboFiltres.Controls.Add(this.gboFiltreSelonStatut);
            this.gboFiltres.Controls.Add(this.gboFiltreTypeAliment);
            this.gboFiltres.Controls.Add(this.gboFiltreBarreRecherche);
            this.gboFiltres.Location = new System.Drawing.Point(13, 59);
            this.gboFiltres.Name = "gboFiltres";
            this.gboFiltres.Size = new System.Drawing.Size(186, 442);
            this.gboFiltres.TabIndex = 12;
            this.gboFiltres.TabStop = false;
            this.gboFiltres.Text = "Filtres";
            // 
            // btnReinitialiserFiltres
            // 
            this.btnReinitialiserFiltres.Location = new System.Drawing.Point(6, 398);
            this.btnReinitialiserFiltres.Name = "btnReinitialiserFiltres";
            this.btnReinitialiserFiltres.Size = new System.Drawing.Size(81, 38);
            this.btnReinitialiserFiltres.TabIndex = 3;
            this.btnReinitialiserFiltres.Text = "Réinitialiser la liste";
            this.btnReinitialiserFiltres.UseVisualStyleBackColor = true;
            this.btnReinitialiserFiltres.Click += new System.EventHandler(this.btnReinitialiserFiltres_Click);
            // 
            // btnFiltrer
            // 
            this.btnFiltrer.Location = new System.Drawing.Point(99, 398);
            this.btnFiltrer.Name = "btnFiltrer";
            this.btnFiltrer.Size = new System.Drawing.Size(81, 38);
            this.btnFiltrer.TabIndex = 2;
            this.btnFiltrer.Text = "Filtrer";
            this.btnFiltrer.UseVisualStyleBackColor = true;
            this.btnFiltrer.Click += new System.EventHandler(this.btnFiltrer_Click);
            // 
            // gboFiltreSelonStatut
            // 
            this.gboFiltreSelonStatut.Controls.Add(this.chkBoxFiltreStatusAliment);
            this.gboFiltreSelonStatut.Location = new System.Drawing.Point(6, 224);
            this.gboFiltreSelonStatut.Name = "gboFiltreSelonStatut";
            this.gboFiltreSelonStatut.Size = new System.Drawing.Size(174, 131);
            this.gboFiltreSelonStatut.TabIndex = 1;
            this.gboFiltreSelonStatut.TabStop = false;
            // 
            // chkBoxFiltreStatusAliment
            // 
            this.chkBoxFiltreStatusAliment.AutoSize = true;
            this.chkBoxFiltreStatusAliment.Location = new System.Drawing.Point(6, 0);
            this.chkBoxFiltreStatusAliment.Name = "chkBoxFiltreStatusAliment";
            this.chkBoxFiltreStatusAliment.Size = new System.Drawing.Size(58, 19);
            this.chkBoxFiltreStatusAliment.TabIndex = 0;
            this.chkBoxFiltreStatusAliment.Text = "Status";
            this.chkBoxFiltreStatusAliment.UseVisualStyleBackColor = true;
            this.chkBoxFiltreStatusAliment.CheckedChanged += new System.EventHandler(this.chkBoxFiltreStatusAliment_CheckedChanged);
            // 
            // gboFiltreTypeAliment
            // 
            this.gboFiltreTypeAliment.Controls.Add(this.cboTypesAliment);
            this.gboFiltreTypeAliment.Controls.Add(this.chkBoxFiltreTypeAliment);
            this.gboFiltreTypeAliment.Location = new System.Drawing.Point(6, 132);
            this.gboFiltreTypeAliment.Name = "gboFiltreTypeAliment";
            this.gboFiltreTypeAliment.Size = new System.Drawing.Size(174, 86);
            this.gboFiltreTypeAliment.TabIndex = 1;
            this.gboFiltreTypeAliment.TabStop = false;
            // 
            // cboTypesAliment
            // 
            this.cboTypesAliment.FormattingEnabled = true;
            this.cboTypesAliment.Location = new System.Drawing.Point(7, 26);
            this.cboTypesAliment.Name = "cboTypesAliment";
            this.cboTypesAliment.Size = new System.Drawing.Size(161, 23);
            this.cboTypesAliment.TabIndex = 1;
            // 
            // chkBoxFiltreTypeAliment
            // 
            this.chkBoxFiltreTypeAliment.AutoSize = true;
            this.chkBoxFiltreTypeAliment.Location = new System.Drawing.Point(6, 0);
            this.chkBoxFiltreTypeAliment.Name = "chkBoxFiltreTypeAliment";
            this.chkBoxFiltreTypeAliment.Size = new System.Drawing.Size(108, 19);
            this.chkBoxFiltreTypeAliment.TabIndex = 0;
            this.chkBoxFiltreTypeAliment.Text = "Types d\'aliment";
            this.chkBoxFiltreTypeAliment.UseVisualStyleBackColor = true;
            this.chkBoxFiltreTypeAliment.CheckedChanged += new System.EventHandler(this.chkBoxFiltreTypeAliment_CheckedChanged);
            // 
            // gboFiltreBarreRecherche
            // 
            this.gboFiltreBarreRecherche.Controls.Add(this.txtRecherche);
            this.gboFiltreBarreRecherche.Controls.Add(this.chkBoxFiltreBarreRecherche);
            this.gboFiltreBarreRecherche.Location = new System.Drawing.Point(6, 28);
            this.gboFiltreBarreRecherche.Name = "gboFiltreBarreRecherche";
            this.gboFiltreBarreRecherche.Size = new System.Drawing.Size(174, 98);
            this.gboFiltreBarreRecherche.TabIndex = 0;
            this.gboFiltreBarreRecherche.TabStop = false;
            // 
            // txtRecherche
            // 
            this.txtRecherche.Location = new System.Drawing.Point(7, 48);
            this.txtRecherche.Name = "txtRecherche";
            this.txtRecherche.Size = new System.Drawing.Size(161, 23);
            this.txtRecherche.TabIndex = 1;
            // 
            // chkBoxFiltreBarreRecherche
            // 
            this.chkBoxFiltreBarreRecherche.AutoSize = true;
            this.chkBoxFiltreBarreRecherche.Location = new System.Drawing.Point(6, 0);
            this.chkBoxFiltreBarreRecherche.Name = "chkBoxFiltreBarreRecherche";
            this.chkBoxFiltreBarreRecherche.Size = new System.Drawing.Size(118, 19);
            this.chkBoxFiltreBarreRecherche.TabIndex = 0;
            this.chkBoxFiltreBarreRecherche.Text = "Nom de l\'aliment";
            this.chkBoxFiltreBarreRecherche.UseVisualStyleBackColor = true;
            this.chkBoxFiltreBarreRecherche.CheckedChanged += new System.EventHandler(this.chkBoxFiltreBarreRecherche_CheckedChanged);
            // 
            // FenetreInventaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 513);
            this.Controls.Add(this.gboFiltres);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblQttyAliment);
            this.Controls.Add(this.numQttyAliment);
            this.Controls.Add(this.btnSupprimerAliment);
            this.Controls.Add(this.lstInventaire);
            this.Controls.Add(this.btnAjouterAliment);
            this.Name = "FenetreInventaire";
            this.Text = "Inventaire";
            this.Load += new System.EventHandler(this.FenetreInventaire_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numQttyAliment)).EndInit();
            this.gboFiltres.ResumeLayout(false);
            this.gboFiltreSelonStatut.ResumeLayout(false);
            this.gboFiltreSelonStatut.PerformLayout();
            this.gboFiltreTypeAliment.ResumeLayout(false);
            this.gboFiltreTypeAliment.PerformLayout();
            this.gboFiltreBarreRecherche.ResumeLayout(false);
            this.gboFiltreBarreRecherche.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAjouterAliment;
        private System.Windows.Forms.ListView lstInventaire;
        private System.Windows.Forms.ColumnHeader colHeaderNom;
        private System.Windows.Forms.ColumnHeader colHeaderStatus;
        private System.Windows.Forms.ColumnHeader colHeaderQtty;
        private System.Windows.Forms.Button btnSupprimerAliment;
        private System.Windows.Forms.ColumnHeader colHeaderPeremption;
        private System.Windows.Forms.NumericUpDown numQttyAliment;
        private System.Windows.Forms.Label lblQttyAliment;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.GroupBox gboFiltres;
        private System.Windows.Forms.GroupBox gboFiltreSelonStatut;
        private System.Windows.Forms.GroupBox gboFiltreTypeAliment;
        private System.Windows.Forms.GroupBox gboFiltreBarreRecherche;
        private System.Windows.Forms.CheckBox chkBoxFiltreStatusAliment;
        private System.Windows.Forms.CheckBox chkBoxFiltreTypeAliment;
        private System.Windows.Forms.CheckBox chkBoxFiltreBarreRecherche;
        private System.Windows.Forms.TextBox txtRecherche;
        private System.Windows.Forms.ComboBox cboTypesAliment;
        private System.Windows.Forms.Button btnFiltrer;
        private System.Windows.Forms.Button btnReinitialiserFiltres;
    }
}