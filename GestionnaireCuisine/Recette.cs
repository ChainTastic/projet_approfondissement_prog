﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GestionnaireCuisine
{
    public class Recette
    {
        #region Attributs

        private string _nom;
        private List<(Aliment, float, UniteMesure)> _lstAlimentsQtty;
        private string _nomImage;

        #endregion Attributs

        #region Accesseurs

        public string Nom
        {
            get { return _nom; }
            set
            {
                if (value == "")
                {
                    throw new ArgumentException("Le nom est vide");
                }

                _nom = value;
            }
        }

        public List<(Aliment, float, UniteMesure)> LstAlimentsQtty
        {
            get { return _lstAlimentsQtty; }
            set { _lstAlimentsQtty = value; }
        }

        public string NomImage
        {
            get { return _nomImage; }
            set { _nomImage = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public Recette(string pNom)
        {
            Nom = pNom;
            _lstAlimentsQtty = new List<(Aliment, float, UniteMesure)>();
        }

        public Recette()
        {
            _lstAlimentsQtty = new List<(Aliment, float, UniteMesure)>();
        }

        #endregion Constructeurs

        #region Méthodes

        /// <summary>
        /// Méthode permettant d'obtenir l'ingrédient de type Aliment de la recette à l'indice donnée en paramètre
        /// </summary>
        /// <param name="pIndice">Indice de l'aliment dans la liste d'ingrédients</param>
        /// <returns>aliment de la recette à l'indice donnée en paramètre</returns>
        public Tuple<Aliment, float, UniteMesure> GetAliment(int pIndice)
        {
            return _lstAlimentsQtty[pIndice].ToTuple();
        }

        /// <summary>
        /// Méthode permettant d'ajouter un ingrédient de type Aliment reçu en paramètre à la recette
        /// </summary>
        /// <param name="tupleAlimentQtty">Tuple composé d'un Aliment et de sa quantité</param>
        public void AjouterAliment((Aliment, float, UniteMesure) tupleAlimentQtty)
        {
            LstAlimentsQtty.Add((tupleAlimentQtty.Item1, tupleAlimentQtty.Item2, tupleAlimentQtty.Item3));
        }

        /// <summary>
        /// Méthode permettant de retirer un ingrédient de type Aliment reçu en paramètre à la recette
        /// </summary>
        /// <param name="tupleAlimentQtty">Tuple composé d'un aliment et de sa quantité</param>
        public void RetirerAliment((Aliment, float, UniteMesure) tupleAlimentQtty)
        {
            LstAlimentsQtty.Remove((tupleAlimentQtty.Item1, tupleAlimentQtty.Item2, tupleAlimentQtty.Item3));
        }

        /// <summary>
        /// Vérifie si chaque ingrédient de la recette est disponible en inventaire
        /// </summary>
        /// <param name="lInventaire">objet de classe Inventaire</param>
        /// <returns>Vrai si chaque ingrédient de la recette sont disponibles dans l'inventaire envoyé en paramètre, Faux autrement</returns>
        public bool VerifierDisponibiliteIngredientsEnInventaire(Inventaire lInventaire)
        {
            foreach ((Aliment, float, UniteMesure) tupleAlimentQtty in this._lstAlimentsQtty)
            {
                if (!lInventaire.AlimentEstDisponible(tupleAlimentQtty.Item1))
                {
                    MessageBox.Show(string.Format("l'aliment {0} de votre recette {1} est indisponible",
                        tupleAlimentQtty.Item1.Nom, Nom));
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Permet de savoir si la recette contient un aliment correspondant au therme reçue en paramètre
        /// </summary>
        /// <param name="thermeRecherche">chaîne de caractères reçue par l'utilisateur</param>
        /// <returns>True si la recette contient un aliment correspondant, False sinon</returns>
        public bool ContientAliment(string thermeRecherche)
        {
            foreach ((Aliment, float, UniteMesure) aliment in LstAlimentsQtty)
            {
                if (aliment.Item1.Nom.ToLower().Contains(thermeRecherche.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Permet de savoir si le nom de la recette contient le therme reçue en paramètre
        /// </summary>
        /// <param name="thermeRecherche">chaîne de caractères reçue par l'utilisateur</param>
        /// <returns>True si le nom de la recette contient la chaîne de caractère reçue en paramètre, False sinon</returns>
        public bool ContientNom(string thermeRecherche)
        {
            if (Nom.ToLower().Contains(thermeRecherche.ToLower()))
            {
                return true;
            }

            return false;
        }

        #endregion Méthodes
    }
}