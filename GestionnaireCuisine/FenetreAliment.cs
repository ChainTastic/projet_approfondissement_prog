﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace GestionnaireCuisine
{
    public partial class FenetreAliment : Form
    {
        #region Attributs

        private Aliment _unAliment;
        private float _quantite;
        private UniteMesure _uniteMesure;

        #endregion Attributs

        #region Accesseurs

        public Aliment UnAliment
        {
            get { return _unAliment; }
            set
            {
                _unAliment = value;
                txtNomAliment.Text = _unAliment.Nom;
                txtDateReception.Text = _unAliment.DateReception.ToString(CultureInfo.CurrentCulture);
                switch (_unAliment.CategorieAliment)
                {
                    case CategorieAliment.FruitEtLegume:
                        optFruitEtLegume.Checked = true;
                        break;

                    case CategorieAliment.DiversSecs:
                        optDivers.Checked = true;
                        break;

                    case CategorieAliment.Oeufs:
                        optOeuf.Checked = true;
                        break;

                    case CategorieAliment.ViandeEtSubstitut:
                        optViandeEtSub.Checked = true;
                        break;

                    case CategorieAliment.ProduitCerealiers:
                        optCerealier.Checked = true;
                        break;

                    case CategorieAliment.ProduitLaitier:
                        optLaitier.Checked = true;
                        break;
                }
            }
        }

        public float QuantiteAliment
        {
            get { return _quantite; }
            set { txtQttyAliment.Text = value.ToString(CultureInfo.CurrentCulture); }
        }

        public UniteMesure UniteMesure
        {
            get { return _uniteMesure; }
            set { cbBoxUniteMesure.SelectedItem = value; }
        }

        #endregion Accesseurs

        #region Constructeurs

        public FenetreAliment()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;
            MaximizeBox = false;
            ShowInTaskbar = false;
            AcceptButton = btnOK;
            CancelButton = btnAnnuler;
            btnOK.DialogResult = DialogResult.OK;
            btnAnnuler.DialogResult = DialogResult.Cancel;
            txtDateReception.Text = DateTime.Today.ToShortDateString();
            optCerealier.Checked = true;
            cbBoxUniteMesure.DataSource = Enum.GetValues(typeof(UniteMesure));
        }

        #endregion Constructeurs

        #region Événements

        //ToDo factorisé le code si possible
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                CategorieAliment laCategorie;

                if (optCerealier.Checked)
                {
                    laCategorie = CategorieAliment.ProduitCerealiers;
                }
                else if (optLaitier.Checked)
                {
                    laCategorie = CategorieAliment.ProduitLaitier;
                }
                else if (optViandeEtSub.Checked)
                {
                    laCategorie = CategorieAliment.ViandeEtSubstitut;
                }
                else if (optDivers.Checked)
                {
                    laCategorie = CategorieAliment.DiversSecs;
                }
                else if (optFruitEtLegume.Checked)
                {
                    laCategorie = CategorieAliment.FruitEtLegume;
                }
                else
                {
                    laCategorie = CategorieAliment.Oeufs;
                }

                if (_unAliment == null)
                {
                    _unAliment = new Aliment(txtNomAliment.Text, laCategorie);
                }
                else
                {
                    _unAliment.Nom = txtNomAliment.Text;
                    _unAliment.CategorieAliment = laCategorie;
                    _unAliment.DateReception = DateTime.Today;
                }
                _quantite = (float)txtQttyAliment.Value;
                Enum.TryParse(cbBoxUniteMesure.SelectedValue.ToString(), out _uniteMesure);
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message);

                if (argumentException.Message.Contains("nom"))
                {
                    txtNomAliment.Focus();
                }

                DialogResult = DialogResult.None;
            }
        }

        #endregion Événements
    }
}